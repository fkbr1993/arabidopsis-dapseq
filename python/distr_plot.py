# Import the necessary packages and modules
import sys
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as spl
import glob
import os

namesRoc = ["roc_distr_best", "roc_distr_oops_mono", "roc_distr_oops_di", "roc_distr_zoops_mono", "roc_distr_zoops_di"];
namesLog = ["logroc_distr_best", "logroc_distr_oops_mono", "logroc_distr_oops_di", "logroc_distr_zoops_mono", "logroc_distr_zoops_di"];

plt.rcParams['lines.linewidth'] = 0.75
plt.suptitle('Delta AUC distribution', fontsize=16)
plt.xlabel('delta', fontsize=16)
plt.ylabel('amount', fontsize=16)

names = []
plotLines = []
cnt = 0;
while cnt < 5:
    x = []
    y = []
    name = namesRoc[cnt]
    data = open("/var/www/bio/distr_coords/" + name + ".txt", "r")
    content = data.readline()

    while content:
        content = content.split()
        x.append(float(content[0]))
        y.append(float(content[1]))
        content = data.readline()

    # Drawing the plot
    plot = plt.plot(x, y, label='aaa')
    plotLines.append(plot)
    names.append(namesRoc[cnt])
    cnt = cnt + 1

try:
    plt.legend([l[0] for l in plotLines], names, loc=1)
    plt.savefig("/var/www/bio/data-processed-server/distr/roc_distr.png", format='png', dpi=400)
except:
    print ""#""Not enough files for this logo!"
plt.clf()
plt.close('all')

plt.rcParams['lines.linewidth'] = 0.75
plt.suptitle('Delta LogAUC distribution', fontsize=16)
plt.xlabel('delta', fontsize=16)
plt.ylabel('amount', fontsize=16)

names = []
plotLines = []
cnt = 0;
while cnt < 5:
    x = []
    y = []
    name = namesLog[cnt]
    data = open("/var/www/bio/distr_coords/" + name + ".txt", "r")
    content = data.readline()

    while content:
        content = content.split()
        x.append(float(content[0]))
        y.append(float(content[1]))
        content = data.readline()

    # Drawing the plot
    plot = plt.plot(x, y, label='aaa')
    plotLines.append(plot)
    names.append(namesLog[cnt])
    cnt = cnt + 1

try:
    plt.legend([l[0] for l in plotLines], names, loc=1)
    plt.savefig("/var/www/bio/data-processed-server/distr/logroc_distr.png", format='png', dpi=400)
except:
    print ""#""Not enough files for this logo!"

plt.clf()
plt.close('all')