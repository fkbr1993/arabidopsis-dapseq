# Import the necessary packages and modules
import sys
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as spl
import glob
import os

namesRoc = ["aucroc_distr_authors_mono_independent", "aucroc_distr_oops_mono_independent", "aucroc_distr_oops_di_independent", "aucroc_distr_zoops_mono_independent", "aucroc_distr_zoops_di_independent"];
namesLog = ["auclogroc_distr_authors_mono_independent", "auclogroc_distr_oops_mono_independent", "auclogroc_distr_oops_di_independent", "auclogroc_distr_zoops_mono_independent", "auclogroc_distr_zoops_di_independent"];

plt.rcParams['lines.linewidth'] = 0.75
plt.suptitle('AUC distribution', fontsize=16)
plt.xlabel('proteins', fontsize=16)
plt.ylabel('roc auc', fontsize=16)

names = []
plotLines = []
cnt = 0;
while cnt < 5:
    x = []
    y = []
    name = namesRoc[cnt]
    data = open("/var/www/bio/distr_coords/" + name + ".txt", "r")
    content = data.readline()

    while content:
        content = content.split()
        x.append(float(content[0]))
        y.append(float(content[1]))
        content = data.readline()

    # Drawing the plot
    plot = plt.plot(x, y, label='aaa')
    plotLines.append(plot)
    names.append(namesRoc[cnt])
    cnt = cnt + 1

try:
    plt.legend([l[0] for l in plotLines], names, loc=4)
    plt.savefig("/var/www/bio/data-processed-server/distr/aucroc_distr_independent.png", format='png', dpi=400)
except:
    print ""#""Not enough files for this logo!"
plt.clf()
plt.close('all')

plt.rcParams['lines.linewidth'] = 0.75
plt.suptitle('LogAUC distribution', fontsize=16)
plt.xlabel('proteins', fontsize=16)
plt.ylabel('logroc auc', fontsize=16)

names = []
plotLines = []
cnt = 0;
while cnt < 5:
    x = []
    y = []
    name = namesLog[cnt]
    data = open("/var/www/bio/distr_coords/" + name + ".txt", "r")
    content = data.readline()

    while content:
        content = content.split()
        x.append(float(content[0]))
        y.append(float(content[1]))
        content = data.readline()

    # Drawing the plot
    plot = plt.plot(x, y, label='aaa')
    plotLines.append(plot)
    names.append(namesLog[cnt])
    cnt = cnt + 1

try:
    plt.legend([l[0] for l in plotLines], names, loc=2)
    plt.savefig("/var/www/bio/data-processed-server/distr/auclogroc_distr_independent.png", format='png', dpi=400)
except:
    print ""#""Not enough files for this logo!"

plt.clf()
plt.close('all')