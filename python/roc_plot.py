# Import the necessary packages and modules
import sys
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as spl
import glob

mode = 'roc'
saveMode = 'roc'

oppositeMode = '.logroc.'
protein = sys.argv[3]

plt.rcParams['lines.linewidth'] = 0.7
plt.suptitle(mode + ' ' + protein, fontsize=16)
if mode == 'logroc':
    plt.xlabel('log(FPR)', fontsize=16)
if mode == 'roc':
    plt.xlabel('FPR', fontsize=16)
plt.ylabel('TPR', fontsize=16)

plt.legend()

names = []
plotLines = []

rocList = glob.glob(sys.argv[1])
rocList.sort()

aucList = glob.glob(sys.argv[2])
aucList.sort()


savePath = sys.argv[4]

for rocFileName in rocList:
    #print rocFileName
    if rocFileName.find(mode, 0) != -1 and rocFileName.find(protein) != -1 and rocFileName.find(oppositeMode, 0) == -1:
        rocSourceMode = '.' + rocFileName.split('/')[-1].split('.')[-4] + '.'
        rocOccurenceMode = '.' + rocFileName.split('/')[-1].split('.')[-3] + '.'
        rocNucleotideMode = '.' + rocFileName.split('/')[-1].split('.')[-2] + '.'

        print rocFileName
        print rocOccurenceMode
        print rocNucleotideMode

        for aucFileName in aucList:
            aucSourceMode = '.' + aucFileName.split('/')[-1].split('.')[-4] + '.'
            aucOccurenceMode = '.' + aucFileName.split('/')[-1].split('.')[-3] + '.'
            aucNucleotideMode = '.' + aucFileName.split('/')[-1].split('.')[-2] + '.'

            if aucFileName.find(protein) != -1 and rocSourceMode == aucSourceMode and rocOccurenceMode == aucOccurenceMode and rocNucleotideMode == aucNucleotideMode:
                print aucFileName
                print aucOccurenceMode
                print aucNucleotideMode

                #print 'protein found: ' + aucFileName + ':' + protein

                aucFile = open(aucFileName, "r")
                content = aucFile.readline()
                content = content.split()

                aucValue = str(round(float(content[1]), 4))

                if aucFileName.find('colamp') != -1:
                    saveMode = 'colamp_vs_col_a.' + mode

                x = []
                y = []

                rocFile = open(rocFileName, "r")
                content = rocFile.readline()

                while content:
                    content = content.split()
                    x.append(float(content[0]))
                    y.append(float(content[1]))
                    content = rocFile.readline()

                # Drawing the plot
                plot = plt.plot(x, y, label='aaa')
                plotLines.append(plot)
                name = rocFileName.split('/')[-1].split('.')[-2] + ' ' + rocFileName.split('/')[-1].split('.')[-3] + ' ' + rocFileName.split('/')[-1].split('.')[-4] + '(auc:' + aucValue + ')'
                names.append(name)

print names
print saveMode
if saveMode == 'colamp_vs_col_a.' + mode:
    try:
        plt.legend([l[0] for l in plotLines], names, loc=4)
        print 'saving: ' + savePath + '/' + protein + '.' + saveMode + '.png';
        plt.savefig(savePath + '/' + protein + '.' + saveMode + '.png', format='png', dpi=150)
    except:
        print ""#""Not enough files for this logo!"