<?php
/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 17.11.17
 * Time: 17:53
 */

namespace lib;


class Bed
{
    public static $format = 'bed';

    /**
     * @var BedRow[]
     */
    public $rows;

    /**
     * @var string
     */
    public $filePath;

    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @param $path
     * @return Bed[]
     */
    public static function loadDir($path)
    {
        $result = [];

        if ($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {

                if ('.' === $file) continue;
                if ('..' === $file) continue;

                if (is_dir($path . '/' . $file)){
                    $result = array_merge($result, self::loadDir($path . '/' . $file));
                } else {
                    $explode = explode('.', $path . '/' . $file);
                    $explodeCnt = count($explode);

                    if ($explode[$explodeCnt-1] == static::$format){
                        $bedFile = new self($path . '/' . $file);
                        $bedFile->loadFile();

                        $result[] = $bedFile;
                    }
                }
            }
            closedir($handle);
        }

        return $result;
    }

    public function loadFile(){
        //log
        //print "loading file: $this->filePath\n";

        $result = [];

        $file_handle = fopen($this->filePath, "r");
        while (!feof($file_handle)) {
            $line = fgets($file_handle);
            $row = new BedRow();
            if ($row->loadLine($line)){
                $result[] = $row;
            }
        }
        fclose($file_handle);

        $this->rows = $result;

        return $this;
    }

    public function saveFile($filePath)
    {
        $data = "";

        foreach ($this->rows as $row){
            $data .= $row->toString() . "\n";
        }

        $explode = explode('/', $filePath);
        $explodeCnt = count($explode);
        unset($explode[$explodeCnt-1]);
        $dirName = implode('/', $explode);
        if (!file_exists($dirName)){
            mkdir($dirName, 0755, true);
        }

        file_put_contents($filePath, $data);

        //log
        print "saved bed to $filePath\n";
    }

    public function sortRowsByField($fieldName, $limit = false, $offset = 0)
    {
        usort($this->rows, function ($rowOne, $rowTwo) use ($fieldName){
            if (1*$rowOne->$fieldName == 1*$rowTwo->$fieldName) {
                return 0;
            }
            return (1*$rowOne->$fieldName > 1*$rowTwo->$fieldName) ? -1 : 1;
        });

        if ($limit){
            $limitedRows = [];
            for ($i = $offset; $i < $limit+$offset; $i++){
                if (!isset($this->rows[$i])) break;
                $limitedRows[] = $this->rows[$i];
            }
            $this->rows = $limitedRows;
        }
    }

    /**
     * @param $sequences Sequence[]
     */
    public function saveFasta(array $sequences, $filePath = null)
    {
        $data = '';

        $cnt = 0;
        foreach ($this->rows as $row){
            $data .= ">ds:200\n";//">100_$row->chrom;_$row->chromStart;_$row->chromEnd\n";
            $data .= $sequences[$row->chrom]->getInterval($row->chromStart, $row->chromEnd - $row->chromStart - 1);
            $data .= "\n";
        }

        $explode = explode('/', $filePath);
        $explodeCnt = count($explode);
        unset($explode[$explodeCnt-1]);
        $dirName = implode('/', $explode);
        if (!file_exists($dirName)){
            mkdir($dirName, 0755, true);
        }

        file_put_contents($filePath, $data);

        //log
        print "saved fasta to $filePath\n";
    }
}