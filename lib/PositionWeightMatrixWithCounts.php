<?php
/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 07.12.17
 * Time: 10:54
 */

namespace lib;


class PositionWeightMatrixWithCounts extends PositionWeightMatrix
{
    public static $format = 'pat';

    public static function processChipMunkFile($pathToResFile)
    {
        $handle = fopen($pathToResFile, "r");
        if ($handle) {
            $KDDC = false;
            $cnt = 1;

            $grabMatrixAndGo = false;
            $grabCnt = 0;
            $matrix = [];

            $grabPWMMatrixAndGo = false;
            $grabPWMCnt = 0;
            $matrixPWM = [];

            while (($line = fgets($handle)) !== false) {

                /*print "FIRSTLY YOU SHOULD FIX MATRIX GRABBING FROM BEGINING OF FILE TO END";
                die();*/

                $cnt++;
                if (strpos($line, 'KDDC|') !== false){
                    $KDDC = $cnt;
                    $kdic = (float)str_replace('KDDC|', '', $line);
                    continue;
                }
                if ($grabPWMMatrixAndGo){
                    $matrixPWM[] = $line;
                    $grabPWMCnt++;
                    if ($grabPWMCnt == 4){
                        $grabPWMMatrixAndGo = false;
                    }
                    continue;
                }
                if ($grabMatrixAndGo){
                    $matrix[] = $line;
                    $grabCnt++;
                    if ($grabCnt == 4){
                        $grabMatrixAndGo = false;
                    }
                    continue;
                }
                if (strpos($line, 'A|') !== false && $KDDC == $cnt-1 && empty($matrix)){
                    $grabMatrixAndGo = true;
                    $matrix[] = $line;
                    $grabCnt++;
                    continue;
                }
                if (strpos($line, 'PWMA|') !== false && $KDDC == $cnt-6){
                    $grabPWMMatrixAndGo = true;
                    $matrixPWM[] = $line;
                    $grabPWMCnt++;
                    continue;
                }

            }

            //var_dump($matrix);
            //var_dump($pathToResFile);
            self::save($matrix, $pathToResFile, 'pcm');
            //var_dump($matrixPWM);
            self::save($matrixPWM, $pathToResFile, 'pwm');

            //save kdic
            /*file_put_contents(
                str_replace(['.mfa', '.result.res'],
                ['', '.kdic.php'], $pathToResFile),
                '<?php return ' . $kdic . ';'
            );*/

            fclose($handle);
        }
    }

    public static function processDiChipMunkFile($pathToResFile)
    {
        $handle = fopen($pathToResFile, "r");
        if ($handle) {
            $KDDC = false;
            $cnt = 1;

            $grabMatrixAndGo = false;
            $grabCnt = 0;
            $matrix = [];

            $grabPWMMatrixAndGo = false;
            $grabPWMCnt = 0;
            $matrixPWM = [];

            while (($line = fgets($handle)) !== false) {

                /*print "FIRSTLY YOU SHOULD FIX MATRIX GRABBING FROM BEGINING OF FILE TO END";
                die();*/

                $cnt++;
                if (strpos($line, 'KDDC|') !== false){
                    $KDDC = $cnt;
                    $kdic = (float)str_replace('KDDC|', '', $line);
                    continue;
                }
                if ($grabPWMMatrixAndGo){
                    $matrixPWM[] = $line;
                    $grabPWMCnt++;
                    if ($grabPWMCnt == 16){
                        $grabPWMMatrixAndGo = false;
                    }
                    continue;
                }
                if ($grabMatrixAndGo){
                    $matrix[] = $line;
                    $grabCnt++;
                    if ($grabCnt == 16){
                        $grabMatrixAndGo = false;
                    }
                    continue;
                }
                if (strpos($line, 'AA|') !== false && $KDDC == $cnt-1 && empty($matrix)){
                    $grabMatrixAndGo = true;
                    $matrix[] = $line;
                    $grabCnt++;
                    continue;
                }
                if (strpos($line, 'PWAA|') !== false && $KDDC == $cnt-18){
                    $grabPWMMatrixAndGo = true;
                    $matrixPWM[] = $line;
                    $grabPWMCnt++;
                    continue;
                }

            }

            //var_dump($matrix);
            var_dump($pathToResFile);
            self::saveDi($matrix, $pathToResFile, 'pcm');
            self::saveDi($matrixPWM, $pathToResFile, 'pwm');
            //var_dump($matrix);
            //var_dump($matrixPWM);
            //die();

            //save kdic
            /*file_put_contents(
                str_replace(['.mfa', '.result.res'],
                ['', '.kdic.php'], $pathToResFile),
                '<?php return ' . $kdic . ';'
            );*/

            fclose($handle);
        }
    }

    public static function saveDi($matrix, $originalPath, $format)
    {
        $explode = explode('.', $originalPath);
        $family = $explode[0];
        $protein = $explode[1];

        $data = ">$family.$protein\n";
        $transMatrix = [];
        foreach ($matrix as $keyOne => $row){
            if ($format == 'pwm'){
                $rowClear = substr($row, 5);
            } else {
                $rowClear = substr($row, 3);
            }

            $explode = explode(' ', $rowClear);

            foreach ($explode as $key => $element){
                $transMatrix[$key][$keyOne] = $element;
            }
        }

        foreach ($transMatrix as $rows) {
            $firstTabInd = true;
            foreach ($rows as $element){
                if ($element == floor($element)){
                    $element = floor($element) . '.0';
                } else {
                    $element = round($element, 3);
                }


                if ($firstTabInd){
                    $data .= "$element";
                    $firstTabInd = false;
                } else {
                    $data .= "\t{$element}";
                }
            }
            $data .= "\n";
        }

        $explode = explode('/', $originalPath);
        $explodeCnt = count($explode);
        unset($explode[$explodeCnt - 1]);
        $dirName = implode('/', $explode);

        if (!file_exists($dirName)){
            mkdir($dirName, 0755, true);
        }

        print "going to save: " . str_replace(['.mfa', '.result.res'], ['', '.' . $format], $originalPath) . "\n";
        print "data: " . $data . "\n";

        file_put_contents(str_replace(['.mfa', '.result.res'], ['', '.' . $format], $originalPath), $data);
        //file_put_contents(str_replace(['.fasta', '.result.res'], ['', '.length.php'], $originalPath), '<?php return ' . count($transMatrix) . ';');
    }

    public static function save($matrix, $originalPath, $format)
    {
        $explode = explode('.', $originalPath);
        $family = $explode[0];
        $protein = $explode[1];

        $data = ">$family.$protein\n";
        $transMatrix = [];
        foreach ($matrix as $keyOne => $row){
            if ($format == 'pwm'){
                $rowClear = substr($row, 5);
            } else {
                $rowClear = substr($row, 2);
            }

            $explode = explode(' ', $rowClear);

            foreach ($explode as $key => $element){
                $transMatrix[$key][$keyOne] = $element;
            }
        }

        foreach ($transMatrix as $rows) {
            $firstTabInd = true;
            foreach ($rows as $element){
                if ($element == floor($element)){
                    $element = floor($element) . '.0';
                } else {
                    $element = round($element, 3);
                }


                if ($firstTabInd){
                    $data .= "$element";
                    $firstTabInd = false;
                } else {
                    $data .= "\t{$element}";
                }
            }
            $data .= "\n";
        }

        $explode = explode('/', $originalPath);
        $explodeCnt = count($explode);
        unset($explode[$explodeCnt - 1]);
        $dirName = implode('/', $explode);

        if (!file_exists($dirName)){
            mkdir($dirName, 0755, true);
        }

        print "going to save: " . str_replace(['.mfa', '.result.res'], ['', '.' . $format], $originalPath) . "\n";
        print "data: " . $data . "\n";

        //file_put_contents(str_replace(['.mfa', '.result.res'], ['', '.' . $format], $originalPath), $data);
        //file_put_contents(str_replace(['.fasta', '.result.res'], ['', '.length.php'], $originalPath), '<?php return ' . count($transMatrix) . ';');
    }

}