<?php
/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 17.11.17
 * Time: 20:36
 */

namespace lib;

/**
 * Class Sequence
 * @package lib
 *
 */
class Sequence
{
    public static $format = 'fasta';

    private $_data;

    public $filePath;
    public $name;

    public function __construct($filePath, $name)
    {
        $this->filePath = $filePath;
        $this->name = $name;
        $this->getData();
    }

    public function getData()
    {
        print "loading sequence: $this->filePath\n";
        $data = '';

        $handle = fopen($this->filePath, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if ($line[0] == '>'){
                    //print "line found: $line";
                    continue;
                }
                $data .= trim($line);
            }

            fclose($handle);
        }

        $this->_data = $data;

        return $this->_data;
    }

    public function getInterval($from, $length)
    {
        //print "getting interval from: $from; length: $length\n";
        return mb_substr($this->_data, $from, $length);
    }

    public static function loadDir($path)
    {
        $result = [];

        if ($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {

                if ('.' === $file) continue;
                if ('..' === $file) continue;

                if (is_dir($path . '/' . $file)){
                    $result = array_merge($result, self::loadDir($path . '/' . $file));
                } else {
                    $explode = explode('.', $path . '/' . $file);
                    $explodeCnt = count($explode);

                    if ($explode[$explodeCnt-1] == static::$format){
                        $sequence = new self($path . '/' . $file, $file);

                        $result[] = $sequence;
                    }
                }
            }
            closedir($handle);
        }

        return $result;
    }
}