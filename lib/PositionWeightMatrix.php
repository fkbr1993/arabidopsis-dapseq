<?php
/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 07.12.17
 * Time: 10:54
 */

namespace lib;


class PositionWeightMatrix
{
    public static $format = 'pwm';

    public $filePath;
    public $data;
    public $nsites;
    public $words;

    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @param $path
     * @return PositionWeightMatrix[]
     */
    public static function loadDir($path)
    {
        $result = [];

        if ($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {

                if ('.' === $file) continue;
                if ('..' === $file) continue;

                if (is_dir($path . '/' . $file)){
                    $result = array_merge($result, self::loadDir($path . '/' . $file));
                } else {
                    $explode = explode('.', $path . '/' . $file);
                    $explodeCnt = count($explode);

                    if ($explode[$explodeCnt-1] == static::$format){
                        $PWMFile = new self($path . '/' . $file);
                        $PWMFile->loadFile();

                        $result[] = $PWMFile;
                    }
                }
            }
            closedir($handle);
        }

        return $result;
    }

    /**
     * @param int $rowsFrom 0-based
     * @return $this
     */
    public function loadFile($rowsFrom = 12){
        //log
        //print "loading file: $this->filePath\n";

        $data = [];

        $file_handle = fopen($this->filePath, "r");
        $cnt = 0;
        while (!feof($file_handle)) {
            $line = trim(fgets($file_handle));
            if ($cnt++ >= $rowsFrom && count(preg_split('/\s+/', $line)) == 4){
                $data[] = preg_split('/\s+/', $line);
            } else if ($cnt == 12){
                $this->nsites = preg_split('/\s+/', $line)[7];
                $this->words = preg_split('/\s+/', $line)[5];
            }
        }
        fclose($file_handle);

        $this->data = $data;

        return $this;
    }

    public function savePWMC($filePath)
    {
        $newData = [];
        foreach ($this->data as $row) {
            $newDataRow = [];
            foreach ($row as $element){
                $newDataRow[] = number_format($element*$this->nsites, 2);
            }
            $newData[] = implode("\t", $newDataRow);
        }
        $newData = implode("\n", $newData);

        $explode = explode('/', $filePath);
        $explodeCnt = count($explode);
        unset($explode[$explodeCnt-1]);
        $dirName = implode('/', $explode);

        unset($explode[0]);
        unset($explode[1]);
        unset($explode[2]);
        unset($explode[3]);
        unset($explode[4]);
        unset($explode[5]);
        unset($explode[$explodeCnt-2]);

        $fileName = implode('.', $explode);

        $newData = ">$fileName\n" . $newData;

        if (!file_exists($dirName)){
            mkdir($dirName, 0755, true);
        }

        file_put_contents($filePath, $newData);

        //log
        print "PWMC to $filePath\n";
        print "$newData\n";
    }
}