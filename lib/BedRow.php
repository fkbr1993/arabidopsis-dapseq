<?php

namespace lib;

/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 17.11.17
 * Time: 17:25
 */
class BedRow
{
    // - Name of the chromosome (or contig, scaffold, etc.).
    public $chrom;

    // - The starting position of the feature in the chromosome or scaffold.
    // The first base in a chromosome is numbered 0.
    public $chromStart;

    // - The ending position of the feature in the chromosome or scaffold.
    // The chromEnd base is not included in the display of the feature.
    // For example, the first 100 bases of a chromosome are defined as
    // chromStart=0, chromEnd=100, and span the bases numbered 0-99.
    public $chromEnd;

    // - Name given to a region (preferably unique). Use "." if no name is assigned.
    public $name;

    // - Indicates how dark the peak will be displayed in the browser (0-1000).
    // If all scores were "'0"' when the data were submitted to the DCC,
    // the DCC assigned scores 1-1000 based on signal value.
    // Ideally the average signalValue per base spread is between 100-1000.
    public $score;

    // - +/- to denote strand or orientation (whenever applicable).
    // Use "." if no orientation is assigned.
    public $strand;

    // - Measurement of overall (usually, average) enrichment for the region.
    public $signalValue;

    // - Measurement of statistical significance (-log10). Use -1 if no pValue is assigned.
    public $pValue;

    // - Measurement of statistical significance using false discovery rate (-log10). Use -1 if no qValue is assigned.
    public $qValue;

    // - Point-source called for this peak; 0-based offset from chromStart. Use -1 if no point-source called.
    public $peak;

    public function loadLine($line)
    {
        $attributes = preg_split('/\s+/', $line);

        if (empty($attributes[0])) return null;

        $this->chrom = $attributes[0];
        $this->chromStart = $attributes[1];
        $this->chromEnd = $attributes[2];
        $this->name = $attributes[3];
        $this->score = $attributes[4];
        $this->strand = $attributes[5];
        $this->signalValue = $attributes[6];
        $this->pValue = $attributes[7];
        $this->qValue = $attributes[8];
        $this->peak = $attributes[9];

        return $this;
    }
    
    public function toString()
    {
        return implode("\t", [
            $this->chrom,
            $this->chromStart,
            $this->chromEnd,
            $this->name,
            $this->score,
            $this->strand,
            $this->signalValue,
            $this->pValue,
            $this->qValue,
            $this->peak,
        ]);
    }
}