<?php
/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 07.02.18
 * Time: 15:08
 */

namespace lib;


class File
{
    /**
     * Without dot
     * @var string
     */
    public static $format;
    
    public $filePath;
    public $ownName;
    public $options;

    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    public static function loadDir($path)
    {
        $result = [];

        if ($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {

                if ('.' === $file) continue;
                if ('..' === $file) continue;

                if (is_dir($path . '/' . $file)){
                    $result = array_merge($result, self::loadDir($path . '/' . $file));
                } else {
                    $explode = explode('.', $path . '/' . $file);
                    $explodeCnt = count($explode);

                    if ($explode[$explodeCnt-1] == static::$format){
                        $fileObj = new self($path . '/' . $file);
                        $fileObj->ownName = $file;

                        $result[] = $fileObj;
                    }
                }
            }
            closedir($handle);
        }

        return $result;
    }
}