<?php
/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 01.05.18
 * Time: 5:09
 */

namespace app\components;

use \yii\helpers\Html as OriginalHtml;


class Html extends \yii\helpers\Html
{
    public static function img($src, $options = [])
    {
        if (!file_exists(__DIR__ . '/../' . 'web' . $src)){
            $src = "/img/no_icon.png";
            $options = ['width' => 100, 'height' => 100];
        }

        return OriginalHtml::a(OriginalHtml::img($src, $options), $src, ['target' => '_blank']);
    }
}