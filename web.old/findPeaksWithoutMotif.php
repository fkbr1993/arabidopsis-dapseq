<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require "../console/lib.php";

$pathPeaks = '/var/www/bio/data/peaks';


$pathMotifsArr = [];
$pathPeaksArr = [];


\lib\NarrowPeak::$format = 'narrowPeak';
$peaks = \lib\NarrowPeak::loadDir($pathPeaks);

foreach ($peaks as $peak){
    $exploded = explode('/', $peak->filePath);
    $exploded[5] = 'motifs';
    unset($exploded[8]);
    unset($exploded[9]);
    unset($exploded[10]);
    $imploded = implode('/', $exploded);
    if (file_exists($imploded)){
        print "For peak $peak->filePath exists motif $imploded<br>";
    } else {
        print "Peak without motif<br>";
    }
}
