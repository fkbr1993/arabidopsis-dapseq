<?php
/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 12.11.17
 * Time: 23:53
 */
?>
<html>
    <head>
        <title>Web gl trying</title>
        <link rel="stylesheet" href="css/site.css">
        <script src="js/three.js"></script>
        <script src="js/main.js"></script>
    </head>
    <body class="text-center">
        <h1>So, let's try it</h1>
        <canvas id="glCanvas" width="640" height="480"></canvas>
    </body>
</html>