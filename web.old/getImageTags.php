<?php

require "console/lib.php";

use \lib\File;

\lib\File::$format = 'png';
$images = \lib\File::loadDir('/var/www/bio/data-processed-server/rocPlots');

function cmp($a, $b)
{
    return strcmp($a->ownName, $b->ownName);
}

usort($images, "cmp");

print "<ul>\n";
/** @var File $image */
foreach($images as $image)
{
    print "<li>\n";
    print "<p>$image->ownName</p>";
    print "<image height='400' width='400' src='/images/rocs/$image->ownName'>\n";
    print "</li>\n\n";
}
print "</ul>\n";

