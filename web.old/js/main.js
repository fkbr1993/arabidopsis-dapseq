window.onload = function(){
    width = window.innerWidth;
    height = window.innerHeight;

    canvas = document.querySelector("#glCanvas");

    canvas.setAttribute('width', width-(width*0.3));
    canvas.setAttribute('height', height-(height*0.3));

    // Initialize the GL context
    const gl = canvas.getContext("webgl");

    // Only continue if WebGL is available and working
    if (!gl) {
        alert("Unable to initialize WebGL. Your browser or machine may not support it.");
        return;
    }

    // Set clear color to black, fully opaque
    //gl.clearColor(0.0, 0.0, 0.0, 1.0);
    // Clear the color buffer with specified clear color
    //gl.clear(gl.COLOR_BUFFER_BIT);

    renderer = new THREE.WebGLRenderer({canvas: canvas});
    renderer.setClearColor(0x3e4e5e);

    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(40, width/height, 0.1, 5000);

    light = new THREE.AmbientLight(0xffffff);
    scene.add(light);

    geometryBigSphere = new THREE.SphereGeometry(300, 16, 16);
    materialBigSphere = new THREE.MeshBasicMaterial({color: 0x00ff00, wireframe: true});
    meshBigSphere = new THREE.Mesh(geometryBigSphere, materialBigSphere);
    scene.add(meshBigSphere);

    geometrySphere = new THREE.SphereGeometry(100, 32, 32);
    materialSphere = new THREE.MeshBasicMaterial({color: 0xfed6f0, wireframe: true});
    meshSphere = new THREE.Mesh(geometrySphere, materialSphere);
    scene.add(meshSphere);

    cntX = 0;
    cntY = 0;
    cntZ = 0;

    cntSp = 0;

    cntDirectionX = true;
    cntDirectionY = true;
    cntDirectionZ = false;
    
    cntDirectionSp = true;

    main({x: 0, y: 0, z: 1000}, {radius: 100});
};




//
// start here
//
function main(cameraPosition, sphere) {
    console.log(cameraPosition.x, cameraPosition.y, cameraPosition.z);

    if (cameraPosition.x > 100){
        cntDirectionX = false;
    }
    if (cameraPosition.x < -50){
        cntDirectionX = true;
    }
    if (cntDirectionX){
        cntX = 0.5;
    } else {
        cntX = -0.5;
    }
    cameraPosition.x += cntX;

    if (cameraPosition.y > 200){
        cntDirectionY = false;
    }
    if (cameraPosition.y < -120){
        cntDirectionY = true;
    }
    if (cntDirectionY){
        cntY = 0.5;
    } else {
        cntY = -0.5;
    }
    cameraPosition.y += cntY;

    if (cameraPosition.z < 800){
        cntDirectionZ = true;
    }
    if (cameraPosition.z > 1500){
        cntDirectionZ = false;
    }
    if (cntDirectionZ){
        cntZ = 1;
    } else {
        cntZ = -1;
    }
    cameraPosition.z += cntZ;

    if (sphere.radius < 50){
        cntDirectionSp = true;
    }
    if (sphere.radius > 300){
        cntDirectionSp = false;
    }
    if (cntDirectionSp){
        cntSp = 1;
    } else {
        cntSp = -1;
    }
    sphere.radius += cntSp;
    meshSphere.geometry = new THREE.SphereGeometry(sphere.radius, 32, 32);




    camera.position.set(cameraPosition.x, cameraPosition.y, cameraPosition.z);
    renderer.render(scene, camera);

    setTimeout(function(){
        main(cameraPosition, sphere);
    }, 1000/30);
}