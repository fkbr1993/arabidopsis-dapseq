<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


require "../console/lib.php";

$path = '/var/www/bio/data-processed-server/authors/roc';

\lib\File::$format = 'roc';
$authors = \lib\File::loadDir($path);

$rows = [];
foreach ($authors as $roc){
    $explode = explode('.', array_reverse(explode('/', $roc->filePath))[0]);

    $family = $explode[1];
    $protein = $explode[2];



    $rocPlot = "/logos/$family.$protein.roc.png";
    $logRocPlot = "/logos/$family.$protein.logroc.png";

    $authorsLogo = "/logos/authors/$family.$protein.png";

    $monoOopsLogo = "/logos/$family.$protein.auto.m.oops.png";

    $monoZoopsLogo = "/logos/$family.$protein.auto.m.1.0.png";

    $diOopsLogo = "/logos/$family.$protein.auto.m.oops.di.png";

    $diZoopsLogo = "/logos/$family.$protein.auto.m.1.0.di.png";

    $rows[] = [
        'family' => $explode[1],
        'protein' => $explode[2],

        'rocPlot' => "/logos/$family.$protein.roc.png",
        'logRocPlot' => "/logos/$family.$protein.logroc.png",

        'authorsLogo' => "/logos/authors/$family.$protein.png",
        'authorsLength' => intval(exec("wc -l '/var/www/bio/data-processed/motifs/$family/$protein/meme_out/meme_m1.PWMC.pcm'")),

        'monoOopsLogo' => "/logos/$family.$protein.auto.m.oops.png",
        'monoOopsLength' => intval(exec("wc -l '/var/www/bio/data-processed-server/motifs/$family.$protein.auto.m.oops.pcm'")),

        'monoZoopsLogo' => "/logos/$family.$protein.auto.m.1.0.png",
        'monoZoopsLength' => intval(exec("wc -l '/var/www/bio/data-processed-server/motifs/$family.$protein.auto.m.1.0.pcm'")),

        'diOopsLogo' => "/logos/$family.$protein.auto.m.oops.di.png",
        'diOopsLength' => intval(exec("wc -l '/var/www/bio/data-processed-server/motifs/di/$family.$protein.auto.m.oops.di.pcm'")),

        'diZoopsLogo' => "/logos/$family.$protein.auto.m.1.0.di.png",
        'diZoopsLength' => intval(exec("wc -l '/var/www/bio/data-processed-server/motifs/di/$family.$protein.auto.m.1.0.di.pcm'")),
    ];
}

function cmpFamily($a, $b)
{
    return strcmp($a['family'], $b['family']);
}

function cmpProtein($a, $b)
{
    return strcmp($a['protein'], $b['protein']);
}

usort($rows, "cmpFamily");
usort($rows, "cmpProtein");

?>
<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="/css/colorbox.css">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/jquery.colorbox.js"></script>

        <style>

        @media print {
            tr:nth-child(even){
                background-color: #9398d833 !important;
                -webkit-print-color-adjust: exact;
            }
            tbody tr:nth-child(odd){
                background-color: #ffe7e78c; !important;
                -webkit-print-color-adjust: exact;
            }
        }}

        .wrapper{
            text-align: center;
        }
        table{
            font-size: 18px;
            border: 2px solid;
            border-radius: 3px;
        }
        tr:nth-child(even){
            background-color: #e4ecce;
        }
        tbody tr:nth-child(odd){
            background-color: #d8ac9333;
        }
        td{
            border: 1px solid;
            padding: 3px;
            border-radius: 2px;
        }
        img{
            width: auto;
            max-width: 340px;
            min-height: 125px;
            height: auto;
            max-height: 350px;
            padding: 10px;

        }
        table {
            display:table;
            margin-right:auto;
            margin-left:auto;
            width:95%;
        }

        td, th {
            display:table-cell;
            text-align:center;
        }
    </style>
    <script>
        jQuery('a.gallery').colorbox();
    </script>

    </head>

    <body>

        <div class="wrapper" style="text-align: center">
            <table>
                <thead style="">
                    <tr style="font-size: 18px">
                        <th>#</th>
                        <th>Family<br>Protein</th>
                        <th>ROC<br>LogROC</th>
                        <!--<th>LogROC</th>-->
                        <th>Logo author</th>
                        <th>Logo mono-oops<br>Logo mono-zoops</th>
                        <!--<th>Logo mono/zoops</th>-->
                        <th>Logo di-oops<br>Logo di-zoops</th>
                        <!--<th>Logo di/zoops</th>-->
                    </tr>
                </thead>
                <tbody style="border: 1px black">
                    <?php $cnt = 1; ?>
                    <?php $wasHeader = false; ?>
                    <?php foreach ($rows as  $row){ ?>
                        <?php if ($cnt != 1 && ($cnt-1) % 2 == 0 && $wasHeader == false) { ?>
                            <?php $wasHeader = true; ?>
                            <thead style="">
                                <tr style="font-size: 18px">
                                    <th>#</th>
                                    <th>Family<br>Protein</th>
                                    <th>ROC<br>LogROC</th>
                                    <!--<th>LogROC</th>-->
                                    <th>Logo author</th>
                                    <th>Logo mono oops<br>Logo mono zoops</th>
                                    <!--<th>Logo mono/zoops</th>-->
                                    <th>Logo di oops<br>Logo di zoops</th>
                                    <!--<th>Logo di/zoops</th>-->
                                </tr>
                            </thead>
                        <?php } else { ?>
                            <?php $wasHeader = false; ?>
                            <tr>
                                <td style="font-size: 20px; text-align: center;"><strong><?= $cnt++; ?></strong></td>
                                <td style="font-size: 16px; text-align: center;"><strong><?= $row['family'] . '<br>' . $row['protein']; ?></strong></td>

                                <td class="roc">
                                    <?php if(!file_exists(__DIR__.'/'.$row['rocPlot'])){ ?>
                                        Нет изображения
                                    <?php } else { ?>
                                        <a class="gallery"  target="_blank" href="<?= $row['rocPlot'];?>">
                                            <img src="<?= $row['rocPlot']; ?>">
                                        </a>
                                    <?php } ?>
                                    <br>
                                    <?php if(!file_exists(__DIR__.'/'.$row['logRocPlot'])){ ?>
                                        Нет изображения
                                    <?php } else { ?>
                                        <a class="gallery"  target="_blank" href="<?= $row['logRocPlot'];?>">
                                            <img src="<?= $row['logRocPlot'];?>">
                                        </a>
                                    <?php } ?>
                                </td>

                                <td>
                                    <?php if(!file_exists(__DIR__.'/'.$row['authorsLogo'])){ ?>
                                        Нет изображения
                                    <?php } else { ?>
                                        Authors length: <?= $row['authorsLength'];?>
                                        <a class="gallery"  target="_blank" href="<?= $row['authorsLogo'];?>">
                                            <img src="<?= $row['authorsLogo'];?>">
                                        </a>
                                    <?php } ?>
                                </td>

                                <td>
                                    <div class="image">
                                        Mono oops length: <?= $row['monoOopsLength'];?>
                                        <?php if(!file_exists(__DIR__.'/'.$row['monoOopsLogo'])){ ?>
                                            Нет изображения
                                        <?php } else { ?>
                                            <a class="gallery"  target="_blank" href="<?= $row['monoOopsLogo'];?>">
                                                <img src="<?= $row['monoOopsLogo'];?>">
                                            </a>
                                        <?php } ?>
                                    </div>
                                    <br>
                                    <div class="image">
                                        Mono zoops length: <?= $row['monoZoopsLength'];?>
                                        <?php if(!file_exists(__DIR__.'/'.$row['monoZoopsLogo'])){ ?>
                                            Нет изображения
                                        <?php } else { ?>
                                            <a class="gallery"  target="_blank" href="<?= $row['monoZoopsLogo'];?>">
                                                <img src="<?= $row['monoZoopsLogo'];?>">
                                            </a>
                                        <?php } ?>
                                    </div>
                                </td>

                                <td>
                                    <div class="image">
                                        Di oops length: <?= $row['diOopsLength'];?>
                                        <?php if(!file_exists(__DIR__.'/'.$row['diOopsLogo'])){ ?>
                                            Нет изображения
                                        <?php } else { ?>
                                            <a class="gallery"  target="_blank" href="<?= $row['diOopsLogo'];?>">
                                                <img src="<?= $row['diOopsLogo'];?>">
                                            </a>
                                        <?php } ?>
                                    </div>
                                    <br>
                                    <div class="image">
                                        Di zoops length: <?= $row['diZoopsLength'];?>
                                        <?php if(!file_exists(__DIR__.'/'.$row['diZoopsLogo'])){ ?>
                                            Нет изображения
                                        <?php } else { ?>
                                            <a class="gallery"  target="_blank" href="<?= $row['diZoopsLogo'];?>">
                                                <img src="<?= $row['diZoopsLogo'];?>">
                                            </a>
                                        <?php } ?>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>

    </body>
</html>
