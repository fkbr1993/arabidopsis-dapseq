<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require "../console/lib.php";

$path = '/var/www/bio/data/motifs';

$PWMPs = \lib\PositionWeightMatrix::loadDir($path);


$rows = [];
foreach ($PWMPs as $matrixP){
    $exploded = explode('/', $matrixP->filePath);

    $family = $exploded[6];
    $protein = $exploded[7];

    $testMotifLogoAutoM = "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.auto.m.png";
    if (!file_exists($testMotifLogoAutoM)){
        continue;
    }

    $testMotifLogoAutoM = "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.auto.m.png";
    $testMotifLogoAutoMLength = require "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.auto.m.length.php";
    $testMotifLogoAutoMKDIC = require "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.auto.m.kdic.php";

    $testMotifLogoUniformM = "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.uniform.m.png";
    $testMotifLogoUniformMLength = require "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.uniform.m.length.php";
    $testMotifLogoUniformMKDIC = require "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.uniform.m.kdic.php";

    $testMotifLogoAutoS = "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.auto.s.png";
    $testMotifLogoAutoSLength = require "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.auto.s.length.php";
    $testMotifLogoAutoSKDIC = require "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.auto.s.kdic.php";

    $testMotifLogoUniformS = "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.uniform.s.png";
    $testMotifLogoUniformSLength = require "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.uniform.s.length.php";
    $testMotifLogoUniformSKDIC = require "/var/www/bio/test-chipmunk/data-processed/motifs/$family.$protein.chr1-5.top600.uniform.s.kdic.php";


    $matrixP->loadFile();

    $our = $exploded;
    $our[4] = 'data-processed';
    $our[9] = 'meme_m1.PWMC.png';
    $our = implode('/', $our);
    $our = str_replace('/var/www/bio', '', $our);
    $ourNewAutoM = str_replace('/var/www/bio', '', $testMotifLogoAutoM);
    $ourNewUniformM = str_replace('/var/www/bio', '', $testMotifLogoUniformM);
    $ourNewAutoS = str_replace('/var/www/bio', '', $testMotifLogoAutoS);
    $ourNewUniformS = str_replace('/var/www/bio', '', $testMotifLogoUniformS);

    $peaks = $exploded;
    $peaks[5] = 'peaks';
    $peaks[8] = 'chr1-5';
    $peaks[9] = 'chr1-5_GEM_events.narrowPeak';
    $peaks = implode('/', $peaks);
    if (file_exists($peaks)){
        $handle = fopen($peaks, "r");

        $peaks = 0;
        while(!feof($handle)){
            $line = fgets($handle);
            $peaks++;
        }
        fclose($handle);
    }else{
        $peaks = 'N/A';
    }


    $row = [
        'family' => $exploded[6],
        'protein' => $exploded[7],
        'dap' => strpos($exploded[7], 'colamp') == false ? 'DAP' : 'ampDAP',
        'nsites' => $matrixP->nsites,
        'peaks' => $peaks,

        'theirLength' => $matrixP->words,
        'our' => $our,

        'ourLengthAutoM' => $testMotifLogoAutoMLength . ' : ' . round($testMotifLogoAutoMKDIC, 3),
        'ourNewAutoM' => $ourNewAutoM,

        'ourLengthUniformM' => $testMotifLogoUniformMLength . ' : ' .  round($testMotifLogoUniformMKDIC, 3),
        'ourNewUniformM' => $ourNewUniformM,

        'ourLengthAutoS' => $testMotifLogoAutoSLength . ' : ' .  round($testMotifLogoAutoSKDIC, 3),
        'ourNewAutoS' => $ourNewAutoS,

        'ourLengthUniformS' => $testMotifLogoUniformSLength . ' : ' .  round($testMotifLogoUniformSKDIC, 3),
        'ourNewUniformS' => $ourNewUniformS,
    ];
    $rows[] = $row;
}

?>
<style>

    @media print {
        tr:nth-child(even){
            background-color: #9398d833 !important;
            -webkit-print-color-adjust: exact;
        }
        tbody tr:nth-child(odd){
            background-color: ##ffe7e78c; !important;
            -webkit-print-color-adjust: exact;
        }
    }}

    .wrapper{
        text-align: center;
    }
    table{
        font-size: 18px;
        border: 2px solid;
        border-radius: 3px;
    }
    tr:nth-child(even){
        background-color: #e4ecce;
    }
    tbody tr:nth-child(odd){
        background-color: #d8ac9333;
    }
    td{
        border: 1px solid;
        padding: 3px;
        border-radius: 2px;
    }
    img{
        width: 350px;
        height: 210px;
        padding: 10px;

    }
</style>

<div class="wrapper">
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Family-Protein</th>
                <th>Nsites</th>
                <th>Peaks</th>

                <th>Motiff</th>
                <th>TheirLength</th>

                <th>Motiff M Auto</th>
                <th>Length M Auto:KDIC</th>

                <th>Motiff S Auto</th>
                <th>Length S Auto:KDIC</th>

                <th>Motiff M Uniform</th>
                <th>Length M Uniform:KDIC</th>

                <th>Motiff S Uniform</th>
                <th>Length S Uniform:KDIC</th>
            </tr>
        </thead>
        <tbody style="border: 1px black">
            <?php $cnt = 1; ?>
            <?php foreach ($rows as  $row){?>
                <tr>
                    <td><?= $cnt++; ?></td>
                    <td><?= $row['family'] . '-' . $row['protein']; ?></td>
                    <td><?= $row['nsites']; ?></td>
                    <td><?= $row['peaks']; ?></td>

                    <td><img src="<?= $row['our']; ?>"></td>
                    <td><?= $row['theirLength']; ?></td>

                    <td><img src="<?= $row['ourNewAutoM']; ?>"></td>
                    <td><?= $row['ourLengthAutoM']; ?></td>

                    <td><img src="<?= $row['ourNewAutoS']; ?>"></td>
                    <td><?= $row['ourLengthAutoS']; ?></td>

                    <td><img src="<?= $row['ourNewUniformM']; ?>"></td>
                    <td><?= $row['ourLengthUniformM']; ?></td>

                    <td><img src="<?= $row['ourNewUniformS']; ?>"></td>
                    <td><?= $row['ourLengthUniformS']; ?></td>
                </tr>
            <?php }?>
        </tbody>
</table>
</div>