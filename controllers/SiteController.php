<?php

namespace app\controllers;

use app\models\Protein;
use app\models\ProteinSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ProteinSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionColVsColamp()
    {
        $searchModel = new ProteinSearch();

        $dataProvider = $searchModel->searchCol(Yii::$app->request->queryParams);

        return $this->render('colVsColamp', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAmp()
    {
        $proteins = Protein::find()->all();

        foreach ($proteins as $protein) {
            print $protein->protein . "<br>";
            $protein->own_name = str_replace(['_col_a', '_colamp_a','_colamp', '_col'], '', $protein->protein);
            if (strpos($protein->protein, 'col_a') !== false || strpos($protein->protein, 'col') !== false){
                $protein->amp = 'col';
                if (strpos($protein->protein, 'colamp') !== false ) {
                    $protein->amp = 'colamp';
                }
            } elseif (strpos($protein->protein, 'colamp') !== false ) {
                $protein->amp = 'colamp';
            } else {
                $protein->amp = 'none';
            }
            $protein->save();
        }

    }

    public function actionDeleteClear()
    {
        $proteins = Protein::find()->all();

        $nonExistsCount = $existsCount = 0;
        foreach ($proteins as $protein) {
            $peaks = __DIR__ . '/../data/peaks/' . $protein->family . '/' . $protein->protein . '/chr1-5/chr1-5_GEM_events.narrowPeak';
            $fileExists = file_exists($peaks);

            if ($fileExists){
                print 'family: ' . $protein->family . 'protein: ' . $protein->protein . "<br>";
                print 'peak: ' . $peaks . "<br>";
                $existsCount++;
            }
        }

        print "URA: $existsCount <br>";

        foreach ($proteins as $protein) {
            $peaks = __DIR__ . '/../data/peaks/' . $protein->family . '/' . $protein->protein . '/chr1-5/chr1-5_GEM_events.narrowPeak';
            $fileExists = file_exists($peaks);

            if (!$fileExists){
                print 'family: ' . $protein->family . 'protein: ' . $protein->protein . "<br>";
                print 'peak doesnt exists: ' . $peaks . "<br>";
                $nonExistsCount++;
                $protein->delete();
            }
        }

        print "URA: $nonExistsCount <br>";

        die();

    }

    public function actionCompareAuc()
    {
        $cntNc = $cntGood = $cntSuperGood = $cntExtraGood = $cntBad = $cntNorm = 0;
        $allGoods = [];

        $proteins = Protein::find()->all();

        $path = "/var/www/bio/data-processed-server/last/auc";

        $files = scandir($path);
        $files = array_diff(scandir($path), array('.', '..'));

        foreach ($proteins as $protein) {
            #print $protein->protein . "<br>";
            $authorRevertedAuc = 1;
            $bestAuc = 0;
            $bestRevertedAuc = 1;

            foreach ($files as $file){

                if (strpos($file, $protein->protein)){
                    #print $file . "<br>";

                    $fh = fopen($path.'/'.$file,'r');
                    $data = '';
                    while ($line = fgets($fh)) {

                        $data = explode("\t", $line);
                        break;
                    }
                    fclose($fh);


                    if (!isset($data[1]) || !isset($data[2])) continue;

                    $rocAucDirty = $data[1];
                    $logRocAucDirty = $data[2];

                    if (strpos($file, 'author') != false){
                        $authorRevertedAuc = 1-$data[1];
                    }

                    $bestAuc = max($data[1], $bestAuc);
                    $bestRevertedAuc = min(1-$data[1], $bestRevertedAuc);

                    #print $rocAucDirty . ":" . $logRocAucDirty ."<br>";
                }
            }

            $aucDelta = $authorRevertedAuc - $bestRevertedAuc;
            $aucDeltaPercent = 100 - ($bestRevertedAuc/$authorRevertedAuc)*100;
            $aucDeltaPercent = (float)$aucDeltaPercent;

            #print "<strong>Author reverted auc: $authorRevertedAuc</strong><br>";
            #print "<strong>Best reverted auc: $bestRevertedAuc</strong><br>";
            #print $protein->protein . "<br>";
            #print "<strong>Best reverted auc delta: $aucDeltaPercent</strong><br>";
            if ($aucDeltaPercent>=100) {
                print "extra good:".$cntExtraGood++ . "<br>";
            } elseif ($aucDeltaPercent>=50 and $aucDeltaPercent<100) {
                print "super good:".$cntSuperGood++ . "<br>";
            } elseif ($aucDeltaPercent>=10 and $aucDeltaPercent<50){
                print "good:".$cntGood++ . "<br>";
            } elseif ($aucDeltaPercent>0 and $aucDeltaPercent<10){
                print "norm:" . $cntNorm++ . "<br>";
            } elseif ($bestAuc != 0){
                print "bad:" . $cntBad++ . "<br>";
            } elseif ($bestAuc == 0) {
                print "not calculated:" . $cntNc++ . "<br>";
            }
        }

        print "extra good:".$cntExtraGood . "<br>";
        print "super good:".$cntSuperGood . "<br>";
        print "good:".$cntGood . "<br>";
        print "norm:" . $cntNorm . "<br>";
        print "bad:" . $cntBad . "<br>";
        print "not calculated:" . $cntNc . "<br>";

        die();
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
