<?php

use app\components\Html;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel \app\models\ProteinSearch */


$this->title = 'Dap-seq Arabidopsis motifs collection';
?>
<div class="site-index">

    <div class="body-content">

        <?php //echo $this->render('_search', ['model' => $searchModel]); ?>


        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => '\yii\grid\SerialColumn'],
                'family',
                'protein',
                [
                    'attribute' => 'colVsColampRocPlot',
                    'value' => function ($data) {
                        return Html::img($data->colVsColampRocPlot, ['width' => 100, 'height' => 100]);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'colVsColampLogRocPlot',
                    'value' => function ($data) {
                        return Html::img($data->colVsColampLogRocPlot, ['width' => 100, 'height' => 100]);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'authorsLogo',
                    'value' => function ($data) {
                        return Html::img($data->authorsLogo, ['width' => 150, 'height' => 70]);
                    },
                    'format' => 'raw',
                ],

                
                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

    </div>
</div>
