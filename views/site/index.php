<?php

use app\components\Html;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel \app\models\ProteinSearch */

$this->title = 'Dap-seq Arabidopsis motifs collection';
?>
<div class="site-index">

    <div class="body-content">

        <?php //echo $this->render('_search', ['model' => $searchModel]); ?>


        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => '\yii\grid\SerialColumn'],
                'family',
                'protein',
                [
                    'attribute' => 'rocPlot',
                    'value' => function ($data) {
                        return Html::img($data->rocPlot, ['width' => 100, 'height' => 100]);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'logRocPlot',
                    'value' => function ($data) {
                        return Html::img($data->logRocPlot, ['width' => 100, 'height' => 100]);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'authorsLogo',
                    'value' => function ($data) {
                        return Html::img($data->authorsLogo, ['width' => 150, 'height' => 70]);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'monoOopsLogo',
                    'value' => function ($data) {
                        return Html::img($data->monoOopsLogo, ['width' => 150, 'height' => 70]);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'monoZoopsLogo',
                    'value' => function ($data) {
                        return Html::img($data->monoZoopsLogo, ['width' => 150, 'height' => 70]);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'diOopsLogo',
                    'value' => function ($data) {
                        return Html::img($data->diOopsLogo, ['width' => 150, 'height' => 70]);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'diZoopsLogo',
                    'value' => function ($data) {
                        return Html::img($data->diZoopsLogo, ['width' => 150, 'height' => 70]);
                    },
                    'format' => 'raw',
                ],
                
                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

    </div>
</div>
