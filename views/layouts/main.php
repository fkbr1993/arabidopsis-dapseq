<?php
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;

\app\assets\AppAsset::register($this);
?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
        <title><?php echo Html::encode($this->title); ?></title>
        <meta property='og:site_name' content='<?php echo Html::encode($this->title); ?>'/>
        <meta property='og:title' content='<?php echo Html::encode($this->title); ?>'/>
        <meta property='og:description' content='<?php echo Html::encode($this->title); ?>'/>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <?php $this->head(); ?>
    </head>
    <body class='wsite-theme-light tall-header-page wsite-page-index weeblypage-index'>
    <?php $this->beginBody(); ?>
    <div id="wrapper">
        <div id="header" class="text-center">
            <h1>DAP-Seq <i>Arabidopsis thaliana</i> motifs collection</h1>
        </div>
        <div id="navigation">
            <?php echo Menu::widget(array(
                'options' => array('class' => 'nav'),
                'items' => array(
                    array('label' => 'Full collection', 'url' => array('/site/index')),
                    array('label' => 'Col vs colamp', 'url' => array('/site/col-vs-colamp')),
                ),
            )); ?>
        </div>
        <div id="content">
            <div id='wsite-content' class='wsite-not-footer'>
                <?php echo $content; ?>
            </div>

        </div>
        <div id="footer">
            <?php echo Html::encode(\Yii::$app->name); ?>
        </div>
    </div>

    <?php $this->endBody(); ?>
    </body>
    </html>
<?php $this->endPage(); ?>