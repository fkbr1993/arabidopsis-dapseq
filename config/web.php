<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'DAP-Seq Arabidopsis thaliana motifs collection',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ghefeUrFY-eTnBTrphWOUy3_V_jtZ96L',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'pattern' => '<page:\d+>',
                    'route' => 'site/index'
                ],
                [
                    'pattern' => '',
                    'route' => 'site/index'
                ],
                [
                    'pattern' => 'col-vs-colamp/<page:\d+>',
                    'route' => 'site/col-vs-colamp'
                ],
                [
                    'pattern' => 'col-vs-colamp',
                    'route' => 'site/col-vs-colamp'
                ],

            ],
        ],
        'html' => [
            'class' => 'app\components\Html',
        ],
    ],
    'params' => $params,
];

return $config;
