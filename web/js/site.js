$(document).ready(function() {
    //$('a.fancy').magnificPopup({type:'image'});
    $('body').magnificPopup({
        delegate: 'a.fancy',
        type: 'iframe',
        closeOnContentClick: false,
        closeBtnInside: true,
        removalDelay: 300,
        mainClass: 'mfp-with-zoom mfp-img-mobile my-mfp-slide-bottom'
    });
});