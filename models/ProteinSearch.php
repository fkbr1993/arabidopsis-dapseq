<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Protein;
use yii\db\Query;
use yii\helpers\VarDumper;

/**
 * ProteinSearch represents the model behind the search form of `app\models\Protein`.
 */
class ProteinSearch extends Protein
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['family', 'protein'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Protein::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'family', $this->family])
            ->andFilterWhere(['like', 'protein', $this->protein]);

        $query->orderBy('family, protein');

        return $dataProvider;
    }

    public function searchCol($params)
    {
        $query = Protein::find();

        $proteins = Protein::find()->where(['amp' => 'colamp'])->all();
        $names = [];
        foreach ($proteins as $protein){
            $names[] = $protein->own_name;
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'amp' => 'col',
        ]);

        $query->andFilterWhere(['like', 'family', $this->family])
            ->andFilterWhere(['own_name' => $names])
            ->andFilterWhere(['like', 'protein', $this->protein])
            ->andFilterWhere(['not', ['like', 'protein', 'colamp']]);

        $query->orderBy('family, protein');

        return $dataProvider;
    }
}
