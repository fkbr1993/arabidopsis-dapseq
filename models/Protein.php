<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "protein".
 *
 * @property int $id
 * @property string $family
 * @property string $protein
 * @property string $own_name
 * @property string $amp
 */
class Protein extends \yii\db\ActiveRecord
{
    public $rocPlot;
    public $logRocPlot;
    public $colVsColampRocPlot;
    public $colVsColampLogRocPlot;
    public $authorsLogo;
    public $monoOopsLogo;
    public $monoZoopsLogo;
    public $diOopsLogo;
    public $diZoopsLogo;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protein';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['family', 'protein', 'own_name', 'amp', ], 'string', 'max' => 255],
            [['family', 'protein'], 'unique', 'targetAttribute' => ['family', 'protein']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'family' => 'Family',
            'protein' => 'Protein',
        ];
    }
    
    function afterFind()
    {

        $this->rocPlot = "/logos/$this->family.$this->protein.roc.png";
        $this->logRocPlot = "/logos/$this->family.$this->protein.logroc.png";

        $this->authorsLogo = "/logos/authors/$this->family.$this->protein.png";

        $this->monoOopsLogo = "/logos/$this->family.$this->protein.auto.m.oops.png";

        $this->monoZoopsLogo = "/logos/$this->family.$this->protein.auto.m.1.0.png";

        $this->diOopsLogo = "/logos/$this->family.$this->protein.auto.m.oops.di.png";

        $this->diZoopsLogo = "/logos/$this->family.$this->protein.auto.m.1.0.di.png";

        $this->colVsColampRocPlot = "/logos/$this->family.$this->own_name.colamp_vs_col_a.roc.png";
        $this->colVsColampLogRocPlot = "/logos/$this->family.$this->own_name.colamp_vs_col_a.logroc.png";

        parent::afterFind(); // TODO: Change the autogenerated stub
    }
}
