<?php

require __DIR__ . "/lib/Sequence.php";


$sequence = new \lib\Sequence($argv[1], '');

function findTheirMotifLength($sequenceName)
{
    $length = 17;

    $explode = explode('/', $sequenceName);

    $family = $explode[2];
    $protein = $explode[3];

    $pathToMotif = "data/motifs/$family/$protein/meme_out/meme_m1.txt";

    $handle = fopen($pathToMotif, "r");
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            $positionOfMotifLength = strpos($line, 'w= ');
            if ($positionOfMotifLength != false){
                $length = 1*substr($line, $positionOfMotifLength+3, 3);
                break;
            }
        }

        fclose($handle);
    }

    return $length;
}


$peakModes = [
    'm',
    /*'s',*/
];

$gcPercents = [
    /*'uniform',*/
    'auto',
];

$weakSites = [
    '1.0',
    'oops'
];

foreach ($peakModes as $peakMode){
    foreach ($gcPercents as $gcPercent){
        foreach ($weakSites as $weakSiteMode){
            if (strpos($sequence->filePath, 'top600-600') !== false) continue;
            $theirLength = findTheirMotifLength($sequence->filePath);

            $explode = explode('/', $sequence->filePath);

            $family = $explode[2];
            $protein = $explode[3];

            $from = $theirLength - 2;
            $to = $theirLength + 2;

            $command = "java " .
                "-Xmx2G " .
                "-cp " .
                "chipmunk.jar " .
                "ru.autosome.ChIPMunk " .
                "$from $to " .
                "no " .
                "$weakSiteMode " .
                "$peakMode:$sequence->filePath " .
                "200 " .
                "20 " .
                "1 " .
                "2 " .
                "random " .
                "$gcPercent " .
                "1>data-processed/motifs/$family.$protein.$gcPercent.$peakMode.$weakSiteMode.result.res " .
                "2>data-processed/motifs/$family.$protein.$gcPercent.$peakMode.$weakSiteMode.log.mes";


            print $command . "\n\n";

            //exec($command);
        }
    }
}