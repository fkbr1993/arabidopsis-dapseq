<?php
/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 20.04.18
 * Time: 3:42
 */

require __DIR__ . "/../lib.php";

\lib\File::$format = 'auc';
$path = "/var/www/bio/data-processed-server/last/auc";
$aucs = \lib\File::loadDir($path);


$aucRocDistrList = [];
$aucLogRocDistrList = [];
$processedAucList = [];

//распределение для oops - mono
$rocOopsMonoList = [];
$logRocOopsMonoList = [];
@unlink("/var/www/bio/roc_distr_oops_mono.txt");
@unlink("/var/www/bio/logroc_distr_oops_mono.txt");

//распределение для oops - di
$rocOopsDiList = [];
$logRocOopsDiList = [];
@unlink("/var/www/bio/roc_distr_oops_di.txt");
@unlink("/var/www/bio/logroc_distr_oops_di.txt");

//распределение для zoops - mono
$rocZoopsMonoList = [];
$logRocZoopsMonoList = [];
@unlink("/var/www/bio/roc_distr_zoops_mono.txt");
@unlink("/var/www/bio/logroc_distr_zoops_mono.txt");

//распределение для zoops - di
$rocZoopsDiList = [];
$logRocZoopsDiList = [];
@unlink("/var/www/bio/roc_distr_zoops_di.txt");
@unlink("/var/www/bio/logroc_distr_zoops_di.txt");

//распределение для best
$rocBestList = [];
$logRocBestList = [];
@unlink("/var/www/bio/roc_distr_best.txt");
@unlink("/var/www/bio/logroc_distr_best.txt");


foreach ($aucs as $auc){
    $explode = explode('.', array_reverse(explode('/', $auc->filePath))[0]);
    $family = $explode[0];
    $protein = $explode[1];
    $occurrence = $explode[2];
    $nucleotide = $explode[3];

    if (isset($processedAucList[$family][$protein])){
        continue;
    } else{
        $processedAucList[$family][$protein] = true;
    }


    foreach ($aucs as $auc2){
        if (strpos($auc2->filePath, $family) !== false && strpos($auc2->filePath, $protein) !== false){
            //проходим по всем ауцам этого белка

            $explode2 = explode('.', array_reverse(explode('/', $auc2->filePath))[0]);
            $family2 = $explode2[0];
            $protein2 = $explode2[1];
            $occurrence2 = $explode2[2];
            $nucleotide2 = $explode2[3];

            $fh = fopen($auc2->filePath,'r');
            $data = '';
            while ($line = fgets($fh)) {

                $data = explode("\t", $line);
                break;
            }
            fclose($fh);


            if (!isset($data[1]) || !isset($data[2])) continue;

            $rocAucDirty = $data[1];
            $logRocAucDirty = $data[2];
            $rocAuc = $rocAucDirty;
            $logRocAuc = $logRocAucDirty;

            $aucRocDistrList[$family][$protein][$occurrence2][$nucleotide2] = $rocAuc;
            $aucLogRocDistrList[$family][$protein][$occurrence2][$nucleotide2] = $logRocAuc;
        }
    }
}

//распределение для oops - mono
foreach ($aucRocDistrList as $familyArr){
    foreach ($familyArr as $proteinArr){
        foreach ($proteinArr as $occurrenceKey => $occurrenceArr){
            if ($occurrenceKey == 'oops'){
                foreach ($occurrenceArr as $nucleotideKey => $nucleotideArr){
                    if ($nucleotideKey == 'mono'){
                        if (isset($proteinArr['oops']['mono']) && isset($proteinArr['authors']['mono'])){
                            $delta = $proteinArr['oops']['mono'] - $proteinArr['authors']['mono'];
                            $delta = round((float)$delta, 3);

                            if ($delta > 0.1) $delta = 0.1;
                            if ($delta < -0.1) $delta = -0.1;

                            $delta = (string)$delta;
                            if (!isset($rocOopsMonoList[$delta])){
                                $rocOopsMonoList[$delta] = 1;
                            } else {
                                $rocOopsMonoList[$delta] = $rocOopsMonoList[$delta] + 1;
                            }
                        }
                    }
                }
            }
        }
    }
}
foreach ($aucLogRocDistrList as $familyArr){
    foreach ($familyArr as $proteinArr){
        foreach ($proteinArr as $occurrenceKey => $occurrenceArr){
            if ($occurrenceKey == 'oops'){
                foreach ($occurrenceArr as $nucleotideKey => $nucleotideArr){
                    if ($nucleotideKey == 'mono'){
                        if (isset($proteinArr['oops']['mono']) && isset($proteinArr['authors']['mono'])){
                            $delta = $proteinArr['oops']['mono'] - $proteinArr['authors']['mono'];
                            $delta = round((float)$delta, 1);

                            if ($delta > 2) $delta = 2;
                            if ($delta < -2) $delta = -2;

                            $delta = (string)$delta;
                            if (!isset($logRocOopsMonoList[$delta])){
                                $logRocOopsMonoList[$delta] = 1;
                            } else {
                                $logRocOopsMonoList[$delta] = $logRocOopsMonoList[$delta] + 1;
                            }
                        }
                    }
                }
            }
        }
    }
}

//распределение для oops - di
foreach ($aucRocDistrList as $familyArr){
    foreach ($familyArr as $proteinArr){
        foreach ($proteinArr as $occurrenceKey => $occurrenceArr){
            if ($occurrenceKey == 'oops'){
                foreach ($occurrenceArr as $nucleotideKey => $nucleotideArr){
                    if ($nucleotideKey == 'di'){
                        if (isset($proteinArr['oops']['di']) && isset($proteinArr['authors']['mono'])){
                            $delta = $proteinArr['oops']['di'] - $proteinArr['authors']['mono'];
                            $delta = round((float)$delta, 3);

                            if ($delta > 0.1) $delta = 0.1;
                            if ($delta < -0.1) $delta = -0.1;

                            $delta = (string)$delta;
                            if (!isset($rocOopsDiList[$delta])){
                                $rocOopsDiList[$delta] = 1;
                            } else {
                                $rocOopsDiList[$delta] = $rocOopsDiList[$delta] + 1;
                            }
                        }
                    }
                }
            }
        }
    }
}
foreach ($aucLogRocDistrList as $familyArr){
    foreach ($familyArr as $proteinArr){
        foreach ($proteinArr as $occurrenceKey => $occurrenceArr){
            if ($occurrenceKey == 'oops'){
                foreach ($occurrenceArr as $nucleotideKey => $nucleotideArr){
                    if ($nucleotideKey == 'di'){
                        if (isset($proteinArr['oops']['di']) && isset($proteinArr['authors']['mono'])){
                            $delta = $proteinArr['oops']['di'] - $proteinArr['authors']['mono'];
                            $delta = round((float)$delta, 1);

                            if ($delta > 2) $delta = 2;
                            if ($delta < -2) $delta = -2;

                            $delta = (string)$delta;
                            if (!isset($logRocOopsDiList[$delta])){
                                $logRocOopsDiList[$delta] = 1;
                            } else {
                                $logRocOopsDiList[$delta] = $logRocOopsDiList[$delta] + 1;
                            }
                        }
                    }
                }
            }
        }
    }
}

//распределение для zoops - mono
foreach ($aucRocDistrList as $familyArr){
    foreach ($familyArr as $proteinArr){
        foreach ($proteinArr as $occurrenceKey => $occurrenceArr) {
            if ($occurrenceKey == 'zoops') {
                foreach ($occurrenceArr as $nucleotideKey => $nucleotideArr) {
                    if ($nucleotideKey == 'mono') {
                        if (isset($proteinArr['zoops']['mono']) && isset($proteinArr['authors']['mono'])) {
                            $delta = $proteinArr['zoops']['mono'] - $proteinArr['authors']['mono'];
                            $delta = round((float)$delta, 3);

                            if ($delta > 0.1) $delta = 0.1;
                            if ($delta < -0.1) $delta = -0.1;

                            $delta = (string)$delta;
                            if (!isset($rocZoopsMonoList[$delta])) {
                                $rocZoopsMonoList[$delta] = 1;
                            } else {
                                $rocZoopsMonoList[$delta] = $rocZoopsMonoList[$delta] + 1;
                            }
                        }
                    }
                }
            }
        }
    }
}
foreach ($aucLogRocDistrList as $familyArr){
    foreach ($familyArr as $proteinArr){
        foreach ($proteinArr as $occurrenceKey => $occurrenceArr) {
            if ($occurrenceKey == 'zoops') {
                foreach ($occurrenceArr as $nucleotideKey => $nucleotideArr) {
                    if ($nucleotideKey == 'mono') {
                        if (isset($proteinArr['zoops']['mono']) && isset($proteinArr['authors']['mono'])) {
                            $delta = $proteinArr['zoops']['mono'] - $proteinArr['authors']['mono'];
                            $delta = round((float)$delta, 1);

                            if ($delta > 2) $delta = 2;
                            if ($delta < -2) $delta = -2;

                            $delta = (string)$delta;
                            if (!isset($logRocZoopsMonoList[$delta])) {
                                $logRocZoopsMonoList[$delta] = 1;
                            } else {
                                $logRocZoopsMonoList[$delta] = $logRocZoopsMonoList[$delta] + 1;
                            }
                        }
                    }
                }
            }
        }
    }
}

//распределение для zoops - di
foreach ($aucRocDistrList as $familyArr){
    foreach ($familyArr as $proteinArr){
        foreach ($proteinArr as $occurrenceKey => $occurrenceArr) {
            if ($occurrenceKey == 'zoops') {
                foreach ($occurrenceArr as $nucleotideKey => $nucleotideArr) {
                    if ($nucleotideKey == 'di') {
                        if (isset($proteinArr['zoops']['di']) && isset($proteinArr['authors']['mono'])) {
                            $delta = $proteinArr['zoops']['di'] - $proteinArr['authors']['mono'];
                            $delta = round((float)$delta, 3);

                            if ($delta > 0.1) $delta = 0.1;
                            if ($delta < -0.1) $delta = -0.1;

                            $delta = (string)$delta;
                            if (!isset($rocZoopsDiList[$delta])) {
                                $rocZoopsDiList[$delta] = 1;
                            } else {
                                $rocZoopsDiList[$delta] = $rocZoopsDiList[$delta] + 1;
                            }
                        }
                    }
                }
            }
        }
    }
}
foreach ($aucLogRocDistrList as $familyArr){
    foreach ($familyArr as $proteinArr){
        foreach ($proteinArr as $occurrenceKey => $occurrenceArr) {
            if ($occurrenceKey == 'zoops') {
                foreach ($occurrenceArr as $nucleotideKey => $nucleotideArr) {
                    if ($nucleotideKey == 'di') {
                        if (isset($proteinArr['zoops']['di']) && isset($proteinArr['authors']['mono'])) {
                            $delta = $proteinArr['zoops']['di'] - $proteinArr['authors']['mono'];
                            $delta = round((float)$delta, 1);

                            if ($delta > 2) $delta = 2;
                            if ($delta < -2) $delta = -2;

                            $delta = (string)$delta;
                            if (!isset($logRocZoopsDiList[$delta])) {
                                $logRocZoopsDiList[$delta] = 1;
                            } else {
                                $logRocZoopsDiList[$delta] = $logRocZoopsDiList[$delta] + 1;
                            }
                        }
                    }
                }
            }
        }
    }
}


//распределение для лучших
//foreach ($aucRocDistrList as $familyArr){
//    foreach ($familyArr as $proteinArr){
//        if (isset($proteinArr['oops']['mono']) && isset($proteinArr['authors']['mono'])){
//            $delta1 = $proteinArr['oops']['mono'] - $proteinArr['authors']['mono'];
//            $delta1 = round((float)$delta1, 3);
//            $delta1 = (string)$delta1;
//        } else {
//            continue;
//        }
//        /*if (isset($proteinArr['oops']['di']) && isset($proteinArr['authors']['mono'])){
//            $delta2 = $proteinArr['oops']['di'] - $proteinArr['authors']['mono'];
//            $delta2 = round((float)$delta2, 3);
//            $delta2 = (string)$delta2;
//        } else {
//            continue;
//        }*/
//        if (isset($proteinArr['zoops']['mono']) && isset($proteinArr['authors']['mono'])){
//            $delta3 = $proteinArr['zoops']['mono'] - $proteinArr['authors']['mono'];
//            $delta3 = round((float)$delta3, 3);
//            $delta3 = (string)$delta3;
//        } else {
//            continue;
//        }
//        /*if (isset($proteinArr['zoops']['di']) && isset($proteinArr['authors']['mono'])){
//            $delta4 = $proteinArr['zoops']['di'] - $proteinArr['authors']['mono'];
//            $delta4 = round((float)$delta4, 3);
//            $delta4 = (string)$delta4;
//        } else {
//            continue;
//        }*/
//
//        $delta = max($delta1, /*$delta2, */$delta3/*, $delta4*/);
//
//        if ($delta > 0.1) $delta = 0.1;
//        if ($delta < -0.1) $delta = -0.1;
//        $delta = (string)$delta;
//
//        if (!isset($rocBestList[$delta])){
//            $rocBestList[$delta] = 1;
//        } else {
//            $rocBestList[$delta] = $rocBestList[$delta] + 1;
//        }
//    }
//}

//foreach ($aucLogRocDistrList as $familyArr){
//    foreach ($familyArr as $proteinArr){
//        if (isset($proteinArr['oops']['mono']) && isset($proteinArr['authors']['mono'])){
//            $delta1 = $proteinArr['oops']['mono'] - $proteinArr['authors']['mono'];
//            $delta1 = round((float)$delta1, 1);
//            $delta1 = (string)$delta1;
//        } else {
//            continue;
//        }
//        /*if (isset($proteinArr['oops']['di']) && isset($proteinArr['authors']['mono'])){
//            $delta2 = $proteinArr['oops']['di'] - $proteinArr['authors']['mono'];
//            $delta2 = round((float)$delta2, 1);
//            $delta2 = (string)$delta2;
//        } else {
//            continue;
//        }*/
//        if (isset($proteinArr['zoops']['mono']) && isset($proteinArr['authors']['mono'])){
//            $delta3 = $proteinArr['zoops']['mono'] - $proteinArr['authors']['mono'];
//            $delta3 = round((float)$delta3, 1);
//            $delta3 = (string)$delta3;
//        } else {
//            continue;
//        }
//        /*if (isset($proteinArr['zoops']['di']) && isset($proteinArr['authors']['mono'])){
//            $delta4 = $proteinArr['zoops']['di'] - $proteinArr['authors']['mono'];
//            $delta4 = round((float)$delta4, 1);
//            $delta4 = (string)$delta4;
//        } else {
//            continue;
//        }*/
//
//        $delta = max($delta1, /*$delta2, */$delta3/*, $delta4*/);
//
//        if ($delta > 2) $delta = 2;
//        if ($delta < -2) $delta = -2;
//        $delta = (string)$delta;
//
//        if (!isset($logRocBestList[$delta])){
//            $logRocBestList[$delta] = 1;
//        } else {
//            $logRocBestList[$delta] = $logRocBestList[$delta] + 1;
//        }
//    }
//}


//распределение для oops - mono
ksort($rocOopsMonoList);
ksort($logRocOopsMonoList);
foreach ($rocOopsMonoList as $coordX => $coordY){
    file_put_contents("/var/www/bio/roc_distr_oops_mono.txt", "$coordX $coordY\n", FILE_APPEND);
}
foreach ($logRocOopsMonoList as $coordX => $coordY){
    file_put_contents("/var/www/bio/logroc_distr_oops_mono.txt", "$coordX $coordY\n", FILE_APPEND);
}

//распределение для oops - di
ksort($rocOopsDiList);
ksort($logRocOopsDiList);
foreach ($rocOopsDiList as $coordX => $coordY){
    file_put_contents("/var/www/bio/roc_distr_oops_di.txt", "$coordX $coordY\n", FILE_APPEND);
}
foreach ($logRocOopsDiList as $coordX => $coordY){
    file_put_contents("/var/www/bio/logroc_distr_oops_di.txt", "$coordX $coordY\n", FILE_APPEND);
}

//распределение для zoops - mono
ksort($rocZoopsMonoList);
ksort($logRocZoopsMonoList);
foreach ($rocZoopsMonoList as $coordX => $coordY){
    file_put_contents("/var/www/bio/roc_distr_zoops_mono.txt", "$coordX $coordY\n", FILE_APPEND);
}
foreach ($logRocZoopsMonoList as $coordX => $coordY){
    file_put_contents("/var/www/bio/logroc_distr_zoops_mono.txt", "$coordX $coordY\n", FILE_APPEND);
}

//распределение для zoops - di
ksort($rocZoopsDiList);
ksort($logRocZoopsDiList);
foreach ($rocZoopsDiList as $coordX => $coordY){
    file_put_contents("/var/www/bio/roc_distr_zoops_di.txt", "$coordX $coordY\n", FILE_APPEND);
}
foreach ($logRocZoopsDiList as $coordX => $coordY){
    file_put_contents("/var/www/bio/logroc_distr_zoops_di.txt", "$coordX $coordY\n", FILE_APPEND);
}

//распределение для best
//ksort($rocBestList);
//ksort($logRocBestList);
//foreach ($rocBestList as $coordX => $coordY){
//    file_put_contents("/var/www/bio/roc_distr_best.txt", "$coordX $coordY\n", FILE_APPEND);
//}
//foreach ($logRocBestList as $coordX => $coordY){
//    file_put_contents("/var/www/bio/logroc_distr_best.txt", "$coordX $coordY\n", FILE_APPEND);
//}

