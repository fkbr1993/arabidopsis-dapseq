<?php
require "/var/www/bio/console.old/lib.php";

use lib\File;

File::$format = 'roc';
$authorRocPath = "/var/www/bio/data-processed-server/authors/roc";
$authorRocs = File::loadDir($authorRocPath);

foreach ($authorRocs as $roc){
    $explode = explode('.', array_reverse(explode('/', $roc->filePath))[0]);
    $mode = $explode[0];
    $family = $explode[1];
    $protein = $explode[2];

    if (strpos($protein, 'colamp') !== false){
        continue;
    }

    if (strpos($protein, '_col_a') !== false){
        $protein = str_replace('_col_a', '', $protein);
    }

    if (strpos($protein, '_col.') !== false){
        $protein = str_replace('_col.', '.', $protein);
    }


    $allTFsList[] = $family.'.'.$protein;

    $thisRocsPath = "/var/www/bio/data-processed-server/last/roc";
    $thisLogRocsPath = "/var/www/bio/data-processed-server/last/logroc";
    $thisAucPath = "/var/www/bio/data-processed-server/last/auc";
    $thisOutPutPath = "/var/www/bio/data-processed-server/logos";

    $commandRoc = "python /var/www/bio/python/roc_plot.py '$thisRocsPath/*.roc' '$thisAucPath/*.auc' $family.$protein $thisOutPutPath";
    $commandLogRoc = "python /var/www/bio/python/logroc_plot.py '$thisLogRocsPath/*.logroc' '$thisAucPath/*.auc' $family.$protein $thisOutPutPath";
    print  $commandRoc . "\n";
    exec($commandRoc);
    print  $commandLogRoc . "\n";
    exec($commandLogRoc);
}

