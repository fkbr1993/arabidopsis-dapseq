<?php

require __DIR__ . "/../lib.php";

$path = '/var/www/bio/data-processed-server/motifs/di';

function loadDir($path)
{

    $result = [];

    if ($handle = opendir($path)) {
        while (false !== ($file = readdir($handle))) {

            if ('.' === $file) continue;
            if ('..' === $file) continue;

            if (is_dir($path . '/' . $file)){
                $result = array_merge($result, loadDir($path . '/' . $file));
            } else {
                $explode = explode('.', $path . '/' . $file);
                $explodeCnt = count($explode);

                if ($explode[$explodeCnt-1] == 'res'){
                    \lib\PositionWeightMatrixWithCounts::processDiChipMunkFile($path . '/' . $file);
                }
            }
        }
        closedir($handle);
    }

    return $result;
}

loadDir($path);