<?php
/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 20.04.18
 * Time: 3:42
 */

require __DIR__ . "/../lib.php";

\lib\File::$format = 'auc';
$path = "/var/www/bio/data-processed-server/last/auc";
$aucs = \lib\File::loadDir($path);


$aucRocDistrList = [];
$aucLogRocDistrList = [];
$processedAucList = [];


foreach ($aucs as $auc){
    $explode = explode('.', array_reverse(explode('/', $auc->filePath))[0]);
    $family = $explode[0];
    $protein = $explode[1];
    $occurrence = $explode[2];
    $nucleotide = $explode[3];

    if (isset($processedAucList[$family][$protein])){
        continue;
    } else{
        $processedAucList[$family][$protein] = true;
    }


    foreach ($aucs as $auc2){
        if (strpos($auc2->filePath, $family) !== false && strpos($auc2->filePath, $protein) !== false){
            //проходим по всем ауцам этого белка

            $explode2 = explode('.', array_reverse(explode('/', $auc2->filePath))[0]);
            $family2 = $explode2[0];
            $protein2 = $explode2[1];
            $occurrence2 = $explode2[2];
            $nucleotide2 = $explode2[3];

            $fh = fopen($auc2->filePath,'r');
            $data = '';
            while ($line = fgets($fh)) {

                $data = explode("\t", $line);
                break;
            }
            fclose($fh);


            if (!isset($data[1]) || !isset($data[2])) continue;

            $rocAucDirty = $data[1];
            $logRocAucDirty = $data[2];
            $rocAuc = $rocAucDirty;
            $logRocAuc = $logRocAucDirty;

            $aucRocDistrList[$protein][$occurrence2][$nucleotide2] = $rocAuc;
            $aucRocDistrList[$protein]['family'] = $family;

            $aucLogRocDistrList[$protein][$occurrence2][$nucleotide2] = $logRocAuc;
            $aucLogRocDistrList['family'] = $family;
        }
    }
}

foreach ($aucRocDistrList as $key => $protein){
    if (!isset($protein['authors']['mono'])){
        unset($aucRocDistrList[$key]);
    }
    if (!isset($protein['oops']['mono']) || !isset($protein['oops']['di'])){
        unset($aucRocDistrList[$key]);
    }
    if (!isset($protein['zoops']['mono']) || !isset($protein['zoops']['di'])){
        unset($aucRocDistrList[$key]);
    }
}

foreach ($aucLogRocDistrList as $key => $protein){
    if (!isset($protein['authors']['mono'])){
        unset($aucLogRocDistrList[$key]);
    }
    if (!isset($protein['oops']['mono']) || !isset($protein['oops']['di'])){
        unset($aucLogRocDistrList[$key]);
    }
    if (!isset($protein['zoops']['mono']) || !isset($protein['zoops']['di'])){
        unset($aucLogRocDistrList[$key]);
    }
}

$aucRocDistrList = array_values($aucRocDistrList);
$aucLogRocDistrList = array_values($aucLogRocDistrList);


function cmpProteinAuthorsMono($a, $b)
{

    return strcmp($a['authors']['mono'], $b['authors']['mono']);
}

function cmpProteinOopsMono($a, $b)
{

    return strcmp($a['oops']['mono'], $b['oops']['mono']);
}

function cmpProteinOopsDi($a, $b)
{

    return strcmp($a['oops']['di'], $b['oops']['di']);
}

function cmpProteinZoopsMono($a, $b)
{

    return strcmp($a['zoops']['mono'], $b['zoops']['mono']);
}

function cmpProteinZoopsDi($a, $b)
{
    return strcmp($a['zoops']['di'], $b['zoops']['di']);
}


$cnt = 0;
$coords = [];

@unlink("/var/www/bio/aucroc_distr_authors_mono_independent.txt");
@unlink("/var/www/bio/aucroc_distr_oops_mono_independent.txt");
@unlink("/var/www/bio/aucroc_distr_oops_di_independent.txt");
@unlink("/var/www/bio/aucroc_distr_zoops_mono_independent.txt");
@unlink("/var/www/bio/aucroc_distr_zoops_di_independent.txt");

usort($aucRocDistrList, "cmpProteinAuthorsMono");
foreach ($aucRocDistrList as $proteinKey => $proteinArr){
    $coordX = $proteinKey;

    $coordY = $proteinArr['authors']['mono'];
    file_put_contents("/var/www/bio/aucroc_distr_authors_mono_independent.txt", "$coordX $coordY\n", FILE_APPEND);
}

usort($aucRocDistrList, "cmpProteinOopsMono");
foreach ($aucRocDistrList as $proteinKey => $proteinArr){
    $coordX = $proteinKey;

    if (isset($proteinArr['oops']['mono'])){
        $coordY = $proteinArr['oops']['mono'];
        file_put_contents("/var/www/bio/aucroc_distr_oops_mono_independent.txt", "$coordX $coordY\n", FILE_APPEND);
    }
}

usort($aucRocDistrList, "cmpProteinOopsDi");
foreach ($aucRocDistrList as $proteinKey => $proteinArr){
    $coordX = $proteinKey;

    if (isset($proteinArr['oops']['di'])){
        $coordY = $proteinArr['oops']['di'];
        file_put_contents("/var/www/bio/aucroc_distr_oops_di_independent.txt", "$coordX $coordY\n", FILE_APPEND);
    }
}

usort($aucRocDistrList, "cmpProteinZoopsMono");
foreach ($aucRocDistrList as $proteinKey => $proteinArr){
    $coordX = $proteinKey;

    if (isset($proteinArr['zoops']['mono'])) {
        $coordY = $proteinArr['zoops']['mono'];
        file_put_contents("/var/www/bio/aucroc_distr_zoops_mono_independent.txt", "$coordX $coordY\n", FILE_APPEND);
    }
}

usort($aucRocDistrList, "cmpProteinZoopsDi");
foreach ($aucRocDistrList as $proteinKey => $proteinArr){
    $coordX = $proteinKey;

    if (isset($proteinArr['zoops']['di'])) {
        $coordY = $proteinArr['zoops']['di'];
        file_put_contents("/var/www/bio/aucroc_distr_zoops_di_independent.txt", "$coordX $coordY\n", FILE_APPEND);
    }
}

@unlink("/var/www/bio/auclogroc_distr_authors_mono_independent.txt");
@unlink("/var/www/bio/auclogroc_distr_oops_mono_independent.txt");
@unlink("/var/www/bio/auclogroc_distr_oops_di_independent.txt");
@unlink("/var/www/bio/auclogroc_distr_zoops_mono_independent.txt");
@unlink("/var/www/bio/auclogroc_distr_zoops_di_independent.txt");

usort($aucLogRocDistrList, "cmpProteinAuthorsMono");
foreach ($aucLogRocDistrList as $proteinKey => $proteinArr){
    $coordX = $proteinKey;

    $coordY = $proteinArr['authors']['mono'];
    file_put_contents("/var/www/bio/auclogroc_distr_authors_mono_independent.txt", "$coordX $coordY", FILE_APPEND);
}

usort($aucLogRocDistrList, "cmpProteinOopsMono");
foreach ($aucLogRocDistrList as $proteinKey => $proteinArr){
    $coordX = $proteinKey;

    if (isset($proteinArr['oops']['mono'])) {
        $coordY = $proteinArr['oops']['mono'];
        file_put_contents("/var/www/bio/auclogroc_distr_oops_mono_independent.txt", "$coordX $coordY", FILE_APPEND);
    }
}

usort($aucLogRocDistrList, "cmpProteinOopsDi");
foreach ($aucLogRocDistrList as $proteinKey => $proteinArr){
    $coordX = $proteinKey;

    if (isset($proteinArr['oops']['di'])) {
        $coordY = $proteinArr['oops']['di'];
        file_put_contents("/var/www/bio/auclogroc_distr_oops_di_independent.txt", "$coordX $coordY", FILE_APPEND);
    }
}

usort($aucLogRocDistrList, "cmpProteinZoopsMono");
foreach ($aucLogRocDistrList as $proteinKey => $proteinArr){
    $coordX = $proteinKey;

    if (isset($proteinArr['zoops']['mono'])) {
        $coordY = $proteinArr['zoops']['mono'];
        file_put_contents("/var/www/bio/auclogroc_distr_zoops_mono_independent.txt", "$coordX $coordY", FILE_APPEND);
    }
}

usort($aucLogRocDistrList, "cmpProteinZoopsDi");
foreach ($aucLogRocDistrList as $proteinKey => $proteinArr){
    $coordX = $proteinKey;

    if (isset($proteinArr['zoops']['di'])) {
        $coordY = $proteinArr['zoops']['di'];
        file_put_contents("/var/www/bio/auclogroc_distr_zoops_di_independent.txt", "$coordX $coordY", FILE_APPEND);
    }
}