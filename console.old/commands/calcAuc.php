<?php
require __DIR__ . "/../lib.php";
use lib\File;

$pwmsDir = realpath(__DIR__ . '/../../test-chipmunk/data-processed/motifs');

File::$format = 'pwm';
$pwms = File ::loadDir($pwmsDir);

$cnt = 1;

/** @var File $pwm */
foreach ($pwms as $pwm){

    $mfa = str_replace(
        ['top600', '.auto', '.uniform', '.m', '.s', '.pwm',],
        ['top600-600', '', '', '', '', ''],
        $pwm->ownName
    );

    $threshold = str_replace(['.pwm'], [], $pwm->ownName);
    $length = require $pwmsDir . '/' . str_replace(['.pwm'], ['.length.php'], $pwm->ownName);
    $result = str_replace(['.pwm'], ['.auc.thr'], $pwm->ownName);

    $command =
        "java 
        -cp programs/sarus/sarus-01Mar2018.jar ru.autosome.SARUS 
        test-chipmunk/data/sequences/top600-600/$mfa.mfa
        $pwm->filePath 
        besthit 
        --skipn | sed 's/,/./'  |
    ruby 
        programs/diHOCOMOCO/calculate_auc.rb 
        $length 
        test-chipmunk/data-processed/thresholds/$threshold.thr 
        mono 
    > test-chipmunk/data-processed/auc/$result";

    $command = str_replace(["\n", "  ", "   "], " ", $command);
    $command = str_replace(["\n", "  ", "   "], " ", $command);
    $command = str_replace(["\n", "  ", "   "], " ", $command);

    print $command . "\n";
    exec($command);
}


/*
puts "find control/control/ -name '#{factor}_#{species}.*.mfa' " + \
             " | xargs -r -n1 -I{} basename -s .mfa '{}' " + \
             " | xargs -r -n1 -I{} echo \"cat control/control/{}.mfa | ruby renameMultifastaSequences.rb '{}'\" " + \
             " | bash " + \
             " | java -cp sarus.jar #{sarus_class} - #{model.pwmh_to_pwm} besthit " + \
             " | ruby calculate_auc.rb #{model.length} #{thresholds_fn} #{model_type} " + \
             " > #{output_fn}"
*/