<?php
require __DIR__ . "/../lib.php";

$path = '/var/www/bio/data-processed-server/motifs/di';

\lib\File::$format = 'dpcm';
$PWMPs = \lib\File::loadDir($path);

/** @var \lib\File $matrixP */
foreach ($PWMPs as $matrixP){
    if (intval(exec("wc -l '$matrixP->filePath'")) < 3){
        continue;
    }
    if (!file_exists(str_replace('.dpcm', '.png', "/var/www/bio/data-processed-server/logos/" . $matrixP->ownName))){
        print("sequence_logo --dinucleotide $matrixP->filePath  --logo-folder /var/www/bio/data-processed-server/logos\n");
        exec("sequence_logo --dinucleotide $matrixP->filePath  --logo-folder /var/www/bio/data-processed-server/logos");
    }
}


$path = '/var/www/bio/data-processed-server/motifs';
\lib\File::$format = 'pcm';
$PWMPs = \lib\File::loadDir($path);
foreach ($PWMPs as $matrixP){
    if (intval(exec("wc -l '$matrixP->filePath'")) < 3){
        continue;
    }
    if (strpos($matrixP->filePath, '.di.') === false){
        print("sequence_logo $matrixP->filePath  --logo-folder /var/www/bio/data-processed-server/logos\n");
        exec("sequence_logo $matrixP->filePath  --logo-folder /var/www/bio/data-processed-server/logos");
    }
}