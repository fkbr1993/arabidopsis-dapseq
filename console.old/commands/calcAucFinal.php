<?php
require __DIR__ . "/../lib.php";
use lib\File;

$pwmsDir = realpath(__DIR__ . '/../../programs/diHOCOMOCO/models/pwm/di/all/');

File::$format = 'dpwm';
$pwms = File ::loadDir($pwmsDir);

$cnt = 1;

/** @var File $pwm */
foreach ($pwms as $pwm){

    $explode = explode('.', array_reverse(explode('/', $pwm->filePath))[0]);
    $family = $explode[0];
    $protein = $explode[1];
    $mode = $explode[2];

    $mfa = "/var/www/bio/programs/diHOCOMOCO/control/control/$family.$protein.top600-600.mfa";
    if (!file_exists($mfa)){
        print  "---File doesn't exists: $mfa---\n";
        continue;
    }

    $threshold = "/var/www/bio/programs/diHOCOMOCO/thresholds/last/di/$family.$protein.$mode.thr";
    if (!file_exists($threshold)){
        print  "---File doesn't exists: $threshold---\n";
        continue;
    }

    $length = $totalLines = intval(exec("wc -l '$pwm->filePath'"));
    $result = "$family.$protein.$mode.di.auc.thr";

    //зачем это? если нет рока или нет логрока, то нечего и  ауц строить!
    /*if (!file_exists("/var/www/bio/data-processed-server/ours/mono/roc/$family.$protein.roc.thr")){
        continue;
    } else if (!file_exists("/var/www/bio/data-processed-server/ours/mono/logroc/$family.$protein.logroc.thr")) {
        continue;
    }*/

    $command =
        "java 
        -cp programs/sarus/sarus-01Mar2018.jar ru.autosome.di.SARUS 
        $mfa
        $pwm->filePath 
        besthit 
        --skipn | sed 's/,/./'  |
    ruby 
        programs/diHOCOMOCO/calculate_auc.rb 
        $length 
        $threshold
    > /var/www/bio/data-processed-server/last/auc/$result";

    $command = str_replace(["\n", "  ", "   "], " ", $command);
    $command = str_replace(["\n", "  ", "   "], " ", $command);
    $command = str_replace(["\n", "  ", "   "], " ", $command);

    print "Going to save $result\n";
    print $command . "\n";
    exec($command);
    //die();
}


/*
puts "find control/control/ -name '#{factor}_#{species}.*.mfa' " + \
             " | xargs -r -n1 -I{} basename -s .mfa '{}' " + \
             " | xargs -r -n1 -I{} echo \"cat control/control/{}.mfa | ruby renameMultifastaSequences.rb '{}'\" " + \
             " | bash " + \
             " | java -cp sarus.jar #{sarus_class} - #{model.pwmh_to_pwm} besthit " + \
             " | ruby calculate_auc.rb #{model.length} #{thresholds_fn} #{model_type} " + \
             " > #{output_fn}"
*/