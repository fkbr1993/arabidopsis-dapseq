<?php

require __DIR__ . "/../lib.php";


$pathToSequences = '/var/www/bio/test-chipmunk/data/sequences/top600';;

\lib\Sequence::$format = 'mfa';
$sequences = \lib\Sequence::loadDir($pathToSequences);

function findTheirMotifLength($sequenceName)
{
    $lengh = 17;

    $explode = explode('.', $sequenceName);

    $family = $explode[0];
    $protein = $explode[1];
    $pathToMotif = "/var/www/bio/data/motifs/$family/$protein/meme_out/meme_m1.txt";

    $handle = fopen($pathToMotif, "r");
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            $positionOfMotifLength = strpos($line, 'w= ');
            if ($positionOfMotifLength != false){
                $lengh = 1*substr($line, $positionOfMotifLength+3, 3);
                break;
            }
        }

        fclose($handle);
    }

    return $lengh;
}


$peakModes = [
    'm',
    /*'s',*/
];

$gcPercents = [
    /*'uniform',*/
    'auto',
];

$weakSites = [
    /*'1.0',*/
    'oops'
];

foreach ($peakModes as $peakMode){
    foreach ($gcPercents as $gcPercent){
        foreach ($weakSites as $weakSiteMode){
            foreach ($sequences as $sequence){
                /*if ($peakMode == 'm' && $gcPercent == 'auto'){
                    continue;
                }*/

                $theirLength = findTheirMotifLength($sequence->name);
                $from = $theirLength - 2;
                $to = $theirLength + 2;

                $command = "java " .
                    "-Xmx2G " .
                    "-cp " .
                    "/var/www/bio/programs/chipmunk/chipmunk7/chipmunk.jar " .
                    "ru.autosome.ChIPMunk " .
                    "$from:$to " .
                    "no " .
                    "$weakSiteMode " .
                    "$peakMode:$sequence->filePath " .
                    "200 " .
                    "20 " .
                    "1 " .
                    "2 " .
                    "random " .
                    "$gcPercent " .
                    "1>/var/www/bio/test-chipmunk/data-processed/motifs/{$sequence->name}.$gcPercent.$peakMode.$weakSiteMode.result.res " .
                    "2>/var/www/bio/test-chipmunk/data-processed/motifs/{$sequence->name}.$gcPercent.$peakMode.$weakSiteMode.log.mes";


                print $command . "\n\n";

                //exec($command);
            }
        }
    }
}