<?php
require __DIR__ . "/../lib.php";
use lib\File;

$mfas = [
    /*'ABI3VP1_tnt.AT5G60130_col_a' => 'ABI3VP1_tnt.AT5G60130_col_a.chr1-5.top600-600.mfa',
    'ARID_tnt.AT1G20910_col_a' => 'ARID_tnt.AT1G20910_col_a.chr1-5.top600-600.mfa',*/
    'bZIP_tnt.TGA3_col' => 'bZIP_tnt.TGA3_col.chr1-5.top600-600.mfa',
    /*'C2C2YABBY_tnt.CRC_col_a' => 'C2C2YABBY_tnt.CRC_col_a.chr1-5.top600-600.mfa',
    'CPP_tnt.AT2G20110_col_a' => 'CPP_tnt.AT2G20110_col_a.chr1-5.top600-600.mfa',*/
];

$pwmsDir = realpath(__DIR__ . '/../../programs/diHOCOMOCO/models/pwm_without_pseudocount/mono/all/authors');

File::$format = 'pwm';
$pwms = File ::loadDir($pwmsDir);

$cnt = 1;


/** @var File $pwm */
foreach ($pwms as $pwm){
    foreach ($mfas as $key => $file){
        if (strpos($pwm->filePath, $key) != false){
            $mfa = $file;
        }
    }


    $threshold = str_replace(['.pwm', 'models/pwm_without_pseudocount/mono/all'], ['.thr', 'thresholds'], $pwm->filePath);
    $length = $totalLines = intval(exec("wc -l '$pwm->filePath'"));
    $result = str_replace(['.pwm'], ['.logroc.thr'], $pwm->ownName);

    $command =
        "java 
        -cp programs/sarus/sarus-01Mar2018.jar ru.autosome.SARUS 
        test-chipmunk/data/sequences/top600-600/$mfa
        $pwm->filePath 
        besthit 
        --skipn | sed 's/,/./'  |
    ruby 
        programs/diHOCOMOCO/calculate_auc.rb 
        $length 
        $threshold 
        mono --print-logroc
    > test-chipmunk/data-processed-background-oops/logroc/$result";

    $command = str_replace(["\n", "  ", "   "], " ", $command);
    $command = str_replace(["\n", "  ", "   "], " ", $command);
    $command = str_replace(["\n", "  ", "   "], " ", $command);

    print $command . "\n";
    /*die();*/
    exec($command);
}


/*
puts "find control/control/ -name '#{factor}_#{species}.*.mfa' " + \
             " | xargs -r -n1 -I{} basename -s .mfa '{}' " + \
             " | xargs -r -n1 -I{} echo \"cat control/control/{}.mfa | ruby renameMultifastaSequences.rb '{}'\" " + \
             " | bash " + \
             " | java -cp sarus.jar #{sarus_class} - #{model.pwmh_to_pwm} besthit " + \
             " | ruby calculate_auc.rb #{model.length} #{thresholds_fn} #{model_type} " + \
             " > #{output_fn}"
*/