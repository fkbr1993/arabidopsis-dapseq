<?php

require __DIR__ . "/../lib.php";

$path = '/var/www/bio/data-processed-server/data-processed/motifs';

function loadDir($path)
{

    $result = [];

    if ($handle = opendir($path)) {
        while (false !== ($file = readdir($handle))) {

            if ('.' === $file) continue;
            if ('..' === $file) continue;
            if ('di' === $file) continue;

            if (is_dir($path . '/' . $file)){
                $result = array_merge($result, loadDir($path . '/' . $file));
            } else {
                $explode = explode('.', $path . '/' . $file);
                $explodeCnt = count($explode);

                if ($explode[$explodeCnt-1] == 'res'){
                    \lib\PositionWeightMatrixWithCounts::processChipMunkFile($path . '/' . $file);
                }
            }
        }
        closedir($handle);
    }

    return $result;
}

loadDir($path);