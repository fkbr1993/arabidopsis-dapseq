<?php

require __DIR__ . "/../lib.php";

$path = realpath(__DIR__ . '/../../programs/diHOCOMOCO/models/pwm/di/all/');

\lib\File::$format = 'dpwm';
$pwms = \lib\File::loadDir($path);

\lib\File::$format = 'txt';
$backgroundPath = '/var/www/bio/programs/diHOCOMOCO/control/local_backgrounds/di';
$backgrounds = \lib\File::loadDir($backgroundPath);

$output = '';

/** @var \lib\File $pwm */
foreach ($pwms as $pwm){
    $backgroundFileExplode = explode('.', array_reverse(explode('/', $pwm->filePath))[0]);
    $backgroundFile = $backgroundFileExplode[0] . '.' . $backgroundFileExplode[1];
    $backgroundFileFound = '';

    $explode = explode('.', array_reverse(explode('/', $pwm->filePath))[0]);
    $family = $explode[0];
    $protein = $explode[1];
    $mode = $explode[2];

    /** @var \lib\File $background */
    foreach ($backgrounds as $background){
        if (strpos($background->filePath, $backgroundFile) !== false){
            $backgroundFileFound = $background->filePath;
        }
    }

    if (!$backgroundFileFound) {
        print "\n No background file: $backgroundFile";
        continue;
    } else {
        //print "\n Background file: $backgroundFileFound";
    }

    $data = [];
    $file = fopen($backgroundFileFound, "r");
    $cnt = 0;
    $background = trim(fgets($file));

    $output = "/var/www/bio/programs/diHOCOMOCO/thresholds/last/di/$family.$protein.$mode.thr";

    if (!file_exists($output) || strlen(file_get_contents($output)) < 2){
        $command = "java 
        -cp programs/chipmunk/chipmunk7/ape-3.0.2.jar 
        ru.autosome.ape.di.PrecalculateThresholds
        $pwm->filePath
        --single-motif
        --background $background
        >
        $output";
        $command = str_replace("\n", " ", str_replace("\n", " ", str_replace("\n", " ", $command)));
        $command = str_replace("  ", " ", str_replace("  ", " ", str_replace("  ", " ", $command)));
        $command = str_replace("  ", " ", str_replace("  ", " ", str_replace("  ", " ", $command)));
        print "\n" . $command . "\n";
        exec($command);
        //die();
    }


}




