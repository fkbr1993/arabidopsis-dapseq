<?php
require __DIR__ . "/../lib.php";

$path = '/var/www/bio/data-processed-server/motifs/di';

\lib\File::$format = 'dpcm';
$PWMPs = \lib\File::loadDir($path);

foreach ($PWMPs as $matrixP){
    exec("ruby /var/www/bio/programs/chipmunk/chipmunk7/dpmflogo3.rb $matrixP->filePath " . str_replace('.pat', '.png', $matrixP->filePath));
}