<?php
$path = '/var/www/bio/test-chipmunk/data-processed-background/roc';

if ($handle = opendir($path)) {
    while (false !== ($file = readdir($handle))) {
        if (strpos($file, 'roc') && !strpos($file, 'png')){
            $explode = explode('.', $file);
            $family = $explode[0];
            $protein = $explode[1];
            $gcPercents = $explode[4];
            $peakMode = $explode[5];

            $input = $path . '/' . $file;
            $output = $path . '/' . $file . '.png';
            $label = "roc\ $family\ $protein\ $gcPercents\ $peakMode";

            if (strpos($file, 'logroc')){
                $label = "logroc\ $family\ $protein\ $gcPercents\ $peakMode";
            }

            $command = "python roc_plot.py $input $output $label";
            print $command . "\n";
            exec($command);
        }
    }
}