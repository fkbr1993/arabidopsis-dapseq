<?php

require __DIR__ . "/../lib.php";



$path = '/var/www/bio/data/motifs';

\lib\PositionWeightMatrix::$format = 'txt';
$PWMPs = \lib\PositionWeightMatrix::loadDir($path);

foreach ($PWMPs as $matrixP){
    $matrixP->savePWMC(str_replace(
        ['/data/', '.txt'],
        ['/data-processed/', '.PWMC.pcm'],
        $matrixP->filePath
    ));
}