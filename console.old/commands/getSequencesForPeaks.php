<?php

require __DIR__ . "/../lib.php";

$path = '/var/www/bio/test-chipmunk/data/peaks';

$pathChr1 = '/var/www/bio/data/sequence/TAIR10/chr1.dna.fasta';
$pathChr2 = '/var/www/bio/data/sequence/TAIR10/chr2.dna.fasta';
$pathChr3 = '/var/www/bio/data/sequence/TAIR10/chr3.dna.fasta';
$pathChr4 = '/var/www/bio/data/sequence/TAIR10/chr4.dna.fasta';
$pathChr5 = '/var/www/bio/data/sequence/TAIR10/chr5.dna.fasta';

$narrowPeaks = \lib\NarrowPeak::loadDir($path);

$sequences = [
    'chr1' => new \lib\Sequence($pathChr1, 'chr1'),
    'chr2' => new \lib\Sequence($pathChr2, 'chr2'),
    'chr3' => new \lib\Sequence($pathChr3, 'chr3'),
    'chr4' => new \lib\Sequence($pathChr4, 'chr4'),
    'chr5' => new \lib\Sequence($pathChr5, 'chr5'),
];



foreach ($narrowPeaks as $narrowPeak)
{
    $narrowPeak->sortRowsByField('qValue', 600, 600);
    $narrowPeak->saveFile(str_replace(
        ['/data/', '.narrowPeak'],
        ['/data-processed/', '.top600-600.narrowPeak'],
        $narrowPeak->filePath
    ));
    $narrowPeak->saveFasta($sequences, str_replace(
        ['/peaks/', '.narrowPeak'],
        ['/sequences/top600-600/', '.top600-600.mfa'],
        $narrowPeak->filePath
    ));
}