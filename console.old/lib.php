<?php
/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 17.11.17
 * Time: 17:40
 */
$libDir = __DIR__ . '/../lib/';

foreach (scandir($libDir) as $filename) {
    $path = $libDir . '/' . $filename;
    if (is_file($path)) {
        require $path;
    }
}