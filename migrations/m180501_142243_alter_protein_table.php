<?php

use yii\db\Migration;

/**
 * Class m180501_142243_alter_protein_table
 */
class m180501_142243_alter_protein_table extends Migration
{
    public $tableName = '{{%protein}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'own_name', $this->string());
        $this->addColumn($this->tableName, 'amp', $this->string());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'own_name');
        $this->dropColumn($this->tableName, 'amp');
    }
}
