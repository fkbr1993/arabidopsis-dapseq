<?php

use yii\db\Migration;

/**
 * Handles the creation of table `protein`.
 */
class m180501_005942_create_protein_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('protein', [
            'id' => $this->primaryKey(),
            'family' => $this->string(),
            'protein' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('protein');
    }
}
